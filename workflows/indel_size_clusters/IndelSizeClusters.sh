#!/bin/bash

# This is a scripted workflow for specific hard-coded genomes. The workflow is described as WORKFLOW 2
# in the accompanying README.
# This script should be modified to accept any .svlen file procuded by ~/scripts/pangenome_graph/variant_analysis/extractAllelesForSample.sh


# Identifies indel size bins and cluster the sequences in each bin by similarity, then plot the size of the largest cluster in every bin

# To run this scirpt, go to /global/homes/e/eugeneg/PANGENOME_GRAPHS/Citrus/minigraph_LimC-LimO-AurM-AurP/IndelSizeClusters/AurM_as_Ref,

# Here, the AurM (Mandarin) haplotype is used as the reference, so all the indel sizes are wrt this reference. 

# see accompanying README for more explanation of some of the steps 


inputPrefix=4_genomes.vcf.svlen

#inputPrefix=$1


for s in {pAurP,pLimC,pLimO}; do
    mkdir ${inputPrefix}.${s}.bin10-seq
    cd ${inputPrefix}.${s}.bin10-seq
    ln -s ../${inputPrefix}.${s} .
    ~/scripts/pangenome_graph/variant_analysis/allelesToFasta_binnedBySize.py  ${inputPrefix}.${s} 10 10

    for f in *fasta; do ~/scripts/pangenome_graph/variant_analysis/get_top_cluster.sh $f; done

    binsize=10
    for ((i=-100000;i<=100000;i=i+$binsize)); do
    	if [[ -f ${inputPrefix}.${s}.bin_${i}.0.fasta.CD-HIT.Cluster0.fasta ]]; then
    	    bin=$i
    	    nBin=$(grep -c '>' ${inputPrefix}.${s}.bin_${i}.0.fasta)
    	    nClust=$(grep -c '>' ${inputPrefix}.${s}.bin_${i}.0.fasta.CD-HIT.Cluster0.fasta)
    	    echo "$bin $nBin $nClust"
    	fi;
    done > Bin_nInBin_nTopCluster
     
    cd ..
done

echo > Bin_nInBin_nTopCluster.gp <<EOF
set xlabel "Indel Size (bin = 10 bp)"
set ylabel "Nr. of indels in bin"
set style fill transparent solid 0.5 noborder
set terminal png size 1400, 1400
set output 'Bin_nInBin_nTopCluster.png'
plot '${inputPrefix}.pAurP.bin10-seq/Bin_nInBin_nTopCluster' u 1:2:(100*($3/$2)) w circles lc rgb "red" ti "SOrg-Pomelo", '${inputPrefix}.pLimC.bin10-seq/Bin_nInBin_nTopCluster'  u 1:2:(100*($3/$2)) w circles lc rgb "blue" ti "LLemon-Citron", '${inputPrefix}.pLimO.bin10-seq/Bin_nInBin_nTopCluster'  u 1:2:(100*($3/$2)) w circles lc rgb "green" ti "LLemon-Orange"
EOF

gnuplot Bin_nInBin_nTopCluster.gp


