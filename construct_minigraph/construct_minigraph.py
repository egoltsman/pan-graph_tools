#!/usr/bin/env python3

import os, getopt, sys
import subprocess
import glob

if 'UTILS_PATH' in os.environ:
    utils_path = os.getenv("UTILS_PATH")
else:
    utils_path = '/global/homes/e/eugeneg/utils'

if 'SCRIPTS_PATH' in os.environ:
    scripts_path = os.getenv("SCRIPTS_PATH")
else:
    scripts_path = '/global/homes/e/eugeneg/scripts/pangenome_graph'


minigraph_bin = utils_path+'/minigraph/minigraph'
    

def run_minigraph(args, outFile):
    '''
    ececute minigraph as a subprocess.  Works with args as a list or a string (the latter is transformed into a list)
    '''
    with open(outFile, "w") as of:
        if isinstance(args, list):
            args = [minigraph_bin] + args
            proc = subprocess.run(args, capture_output=True, text=True)
        else:
            cmd = minigraph_bin + ' ' + args
            proc = subprocess.run(cmd, capture_output=True, text=True, shell=True)

        print(proc.stderr)
        proc.check_returncode()
        of.write(proc.stdout)

def add_paths(fasta, gfa, gAf):

    #first re-run minigraph in mapping mode
    sampleName = inputD[fasta]
    print("Mapping %s (sample %s) back to the graph.." % (fasta, sampleName))
    args = ['-t', '36', '--vc', '-x' 'asm', gfa, fasta]
    run_minigraph(args, gAf) 

    #now import aligments as paths
    gfaTemp = gfa + '.tmp'
    args = [scripts_path+'/add_gaf_paths_to_gfa.py', '-a', gAf, '-g', gfa, '-o', gfaTemp, '-m', '3', '-N', sampleName ]
    proc = subprocess.run(args, capture_output=True, text=True)
    print(proc.stderr)
    proc.check_returncode()

    #overwrite the input gfa with the new 
    os.rename(gfaTemp, gfa)



if __name__ == "__main__":
    
    baseGfa = None
    multiSampleFasta = False
    threads = '8'
    buildPaths = True  #disable this if you don't want to map the sequences back to the created graph and derive paths from that maping
    
    script=os.path.basename(sys.argv[0])
    usageStr='usage: ' + script + ' -i [list of files/samples] <<-g [base gfa file]>> <<-M [multi-sample fasta?] <<-t threads>>'

    if len(sys.argv) < 3:
        print(usageStr)
        sys.exit(2)
    try:
        opts, extras = getopt.getopt(sys.argv[1:], 'i:g:t:M')
        for opt, arg in opts:
            if opt == '-i':
                fof = arg
            if opt == '-g':
                baseGfa = arg
            if opt == '-t':
                threads = arg
            if opt == '-M':
                multiSampleFasta = True

    except getopt.error as err: 
        print (str(err)) 
        sys.exit(2)

                

                
    fof_content = open(fof).readlines()    
    inputD = {}
    fastas = []
    for line in fof_content:
        cols = line.rstrip().split()
        if not len(cols):
            continue
        if len(cols) == 2:
            fa, acc = cols
            if acc in inputD.values():
                raise Exception("It looks like the same accession has multiple entries in the input fof - this is not allowed!")
            inputD[fa] = acc
        else:
            fa = cols[0]
        fastas.append(fa)


    nFastas = len(fastas)
    
    if nFastas > 1 and multiSampleFasta:
        raise Exception('The multi-sample fasta flag -M only makes sense when a single fasta file is passed in, but you have multiple fasta files in the input list. Aborting.')
    

    if nFastas > 1:
        gfa = str(nFastas) + '_genomes.GFA'
        fastaFStr = ' '.join(fastas)
    elif nFastas == 1:
        fastaF = fastas[0]
        mySuffix = fastaF.split('.')[-1]
        gfa = fastaF.replace(mySuffix, 'GFA')
        if multiSampleFasta:
            myFa = fastas[0]
            print('Treating input as a multi-sample fasta file (one fasta record = one sample)')
            subprocess.check_output(['/global/homes/e/eugeneg/scripts/formatters_and_filters/splitfasta.pl', myFa, '1'])
            splitFaList = glob.glob(myFa+'_*[0-9]')
            print('Split resulted in %d fasta files' % (len(splitFaList)))
            fastaFStr = ' '.join(splitFaList)
        else:
            fastaFStr = fastas[0]
    else:
        raise Exception('No fastas read from the input file')

    # construct graph
    mgArgs = ' -x ggs -t ' + threads
    if baseGfa:
        mgArgs += ' ' + baseGfa + ' ' + fastaFStr
    else:
        mgArgs += ' ' + fastaFStr
    run_minigraph(mgArgs, gfa)
    

    if buildPaths:
        #add paths iteratively, overwriting previous gfa file with new one
        for fasta in inputD.keys():
            accessionName=inputD[fasta]
            gAf =  gfa.replace('GFA', 'gaf')
            gAf += '.' + accessionName 
            add_paths(fasta, gfa, gAf)
            
    print('Done.')


        
