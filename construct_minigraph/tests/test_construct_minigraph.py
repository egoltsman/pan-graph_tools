import unittest
import construct_minigraph

class TestRunner(unittest.TestCase):
    '''
    test class for the graph contstruciont module in  construct_graph.minigraph.py
    '''
    def setUp(self):
        self.threads = 2
        self.args = ['-x', 'ggs', '-t', str(self.threads)]
        self.gfaIn ='tests/fooSeq.GFA'
        self.testFa1 = 'tests/fooSeq.fasta'
        self.testFa2 = 'tests/barSeq.fasta'
        
    def test_fasta_only(self):
        myOut = 'tests/test1.GFA'
        myArgs = self.args + [self.testFa1 , self.testFa2]
        construct_minigraph.run_minigraph(myArgs, myOut) 
        f = open(myOut,  "r")
        self.assertTrue( 'S\t' in f.read() )
        f.close()
        
    def test_fasta_and_gfa(self):
        myOut = 'tests/test2.GFA'
        myArgs = self.args + [self.gfaIn, self.testFa2] 
        construct_minigraph.run_minigraph(myArgs, myOut) 
        f = open(myOut,  "r")
        self.assertTrue ( 'S\t' in f.read() )
        f.close()

if __name__ == '__main__':
    unittest.main()
