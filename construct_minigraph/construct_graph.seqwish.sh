#!/bin/bash
set -e

fastaIn=$1
gfaOut=`echo $fastaIn | sed 's|\.fa.*||'`  #strips .fa or .fasta
gfaOut=$gfaOut.GFA

nRecords=`grep -c '>' $fastaIn`
inputStr=$fastaIn
#split the all-in-one fasta into individual files
if [[ $nRecords -gt 1 ]]
then
    while true; do
	  read -p "Input fasta contains $nRecords entires. Do you want to split it and treat each entry as an individual sample?" yn
	  case $yn in
		[Yy]* ) ~/scripts/formatters_and_filters/splitfasta.pl $fastaIn 1 && inputStr=`ls ${inputStr}_*[0-9] | tr "\n" " " ` && echo "Split resulted in files $inputStr"; break;;
		[Nn]* ) echo "Will process multiple fasta records as part of one sampl...e"; break;;
		* ) echo "Please answer yes or no.";;
	  esac
    done

fi

echo $inputStr

#align
pafOut=`echo $gfaOut | sed 's|GFA|paf|'`
#use large (50kb) alignmet block per EG's recommendation
${UTILS_PATH}/vgteam/edyeet/edyeet -t 8 -X -s 50000 "$inputStr" "$inputStr"  > $pafOut



#construct 

# filter out containment alignments and those under 10kb
pafFiltered=$pafOut.fpa-drop-10kb+.paf
source activate fpa
cat $pafOut | fpa -o $pafFiltered drop -c -l 10000
conda deactivate


#run seqwish on (or direct ouput to) cscratch!  (seg faults on projectb - space quota???)
${UTILS_PATH}/seqwish/bin/seqwish -t 8 -s $fastaIn -p $pafFiltered -g $gfaOut -P
#mv $SCRATCH/$gfaOut .

#smooth
${UTILS_PATH}/vgteam/smoothxg/bin/smoothxg -t 12 -g $gfaOut > $gfaOut.smoothxg.GFA




