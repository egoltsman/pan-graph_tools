

Pan-graph_tools
================

This is a collection of helper scripts to work with pan-genome graphs, primarily in GFA/GFA2 formats.




## set up

Many rely on the gfapy python API which is avalable in this conda environment:

/global/homes/e/eugeneg/.conda/envs/gfa+

Before using the scripts, set up the environment by executing
. ./env.sh



If this environment is not available/visible to you or if it causes conflicts with your current environment, you should create your own
base conda environment and install the gfapy package into it:

conda create --name some_name
source activate some_name
conda install -c bioconda gfapy

Alternatively, use pip:
  pip install gfapy




Scripts
=======


## graph construction 

The following programs are available for working with pangenome graphs in GFA format. Most have been tested with graphs created by the minigraph utility.
Other de-novo graph construction utilities include pggb and vg, both of which provide the conversion functionality to produce GFA-formatted graphs.

* construct_minigraph.py

   > Creates an alignment-based graph from a list of input fasta files and corresponding sample/chromosome/contig names.  It uses minigraph to build the graph, then traces sequence-coherent paths through the graph by aligning the original sequences back to it.

   > Before running the workflow, verify that everything is set up correctly by running this unit test from the construct_minigraph/ directory:
   
   python -m unittest tests.test_construct_minigraph



## variant analysis 

* variant_analysis/call_variants_on_GFA.sh

  > Converts a GFA graph to vg format and runs the variant calling workflow involving bubble calling and allele-to-sequence assignment


* variant_analysis/extract_alleles/extract_alleles_for_sample.sh

  > Reads a vg-produced vcf file and extracts the length of the alternative allele for the non-zero genotypes matching the selected sequence/prefix,
  > then prints out the *difference* vs the reference allele as the "length" of the SV.  The output is printed in the form of a set of of fasta sequences -
  > one for the referenc allele and one for every alt allese associated with a given bubble (vcf entry)


* variant_analysis/extract_alleles/allelesToFasta_sizeRange.py  & allelesToFasta_binnedBySize.py

  > Reads the "allele file" produced by extract_alleles_for_sample.sh and performs binning and fitering on the results.


* variant_analysis/get_top_cluster.sh

  > Performs sequence similarity clustering on the input sequences (presumably indel alleles) and returns the sequences in the most abundant cluster.
                 

* variant_analysis/blast_representative_vs_ncbi.sh

  > Extracts the representitive sequence name  from CD-HIT output (.clstr file) and does blastn against NCBI 


* variant_analysis/vg-vcf_to_gff3.pl

  > This program was originally obtained from https://github.com/abrahamdsl/vcf_to_gff3 and then modified by me for use with vg-produced vcfs ONLY !


* variant_analysis/snarl_coords.py

  > Given a vg-produced (!) vcf file of variants and the corresponding GFA file with paths containing query start coordinate (qs:Z)  and offset (of:Z) tags,
  > extrapolates the variant coordinates *on the sample-sequences*  (as opposed to coords on the path as produced by vg).  The tags are normally placed into the GFA file
  > duding the path mapping step in construct_minigraph.py.  Outputs csv | vcf | bed

* master_variant_table.py

  > Builds a generic master table to store various metadata pertaining to each variant (bubble).  The initial variant info is parsed out of a vg-produced vcf file. 
  > Once the table is created, additional data can be added to the variants by passing in a csv file containing exactly TWO columns, with the first column containing the variant 
  > IDs already present in the table and the second column containing the data to add.  One example of such csv file is produced by snarl_coords.py mentioned above.



## GFA exploration/editing 

The way meta-data is added and stored curretnly is via 'Fragment' entries in a GFAv2 formatted file.  This format is not ideal for graph exploration, and the programs below
are pretty slow and just serve as proof-of-concept tool to evaluate toy graphs. These tools rely heavily on the gfapy python package which can be installed/loaded via the
'gfa+' conda environment mentioned above

* gfa_convert.py

  > Converts between GFAv1 and GFAv2 formats
  

* add_defline_to_gff.py

  > Adds gene/protein deflines as 'Note' attributes to gff file. Outputs updated gff.
  

* add_annotation_to_gfa.py

  > Adds annotations from the gff file as Fragments to graph Segments that appear in the paths traced by the specified sample. Outputs GFA2


* add_gaf_paths_to_gfa.py

  > Reads gaf alignment file produced by minigraph and add paths of minimum size into given GFA file
  
  
* add_snarl-traversals_to_gfa.py

  > Adds vg-produced bubble (snarl) data to the graph.  Given a graph (GFA2) and a list of snarls and snarl traversals from vg snarls (json
  > format; use 'vg view' to convert from binary to json),  adds the traversals as new sub-paths. The sub-path line has a unique id and is tagged
  > with names of the parent sample-paths.   Outputs: GFA2
  
  > Common traversal "tips" allow to assign a set of traversals to their original unique snarls, allowing to create snarl ids which we can then add as tags to the variant sub-paths:

     P   7295<>7297_1   7295+,8927+,7296+,8928+,7297+  pp:Z:pBd1_1.3,pRef.2  np:i:2  sn:Z:7295<>7297   si:J:{"directed_acyclic_net_graph": true, "end": {"node_id": "7297"}, "start": {"node_id": "7295"}, "start_end_reachable": true, "type": 1}

  > Additionally, loads snarl and traversal data into a pandas data frame and exports it in a csv file.



* variant_analyis/add_variant_from_vcf_to_path_coords.py

  > Lifts variant calls from a vcf file and adds them as Fragments to GFA graph. Outputs GFA2.



* variant_analysis/explore_variantion_in_gfa.py

  > A multi-functional program that summarizes various metrics associated with bubbles (aka snarls) in a graph.  Relies on data types created by the  add_*.py  tools
  described above.
  




## Variant validation 

* variant_analysis/variant_validation/validate_against_ground.py

   > Compares variant calls against a ground truth set and reports precision and recall statistics
   > his script was designed to work with the ouput of the Visor variant simulation package, but will work with any pair of bed files in the format '[seq_id]\t[variant_start_pos]\t[variant_end_pos]'
   
   > Use the accompanying wrapper script 'run_vallidation.sh" to validate vg-produced variant calls against a Visor-produced simulated ground_truth.
   > (Remember, vg reports positions on paths, so you need to either make sure those actually correspond to the original sequence coordinates. If not, they must be corrected/offset accordingly before runnign this workflow) 
   




