#!/usr/bin/env python3


# Adds gene/protein deflines as 'Note' attributes to gff file. Outputs updated gff.
#
# The input deflines can be in the following formats
# 
# [gene_name].[0-9]+  defLine [gene description]
# [gene_name].[0-9]+  pdef   [protein description]
#
# e.g.:
# Bradi1g00438.1  pdef    (1 of 15) 5.99.1.2 - DNA topoisomerase / Untwisting enzyme
# Bradi1g00450.1  defLine Rab GTPase activator activity (Blast2GO)

# These could be in any combination, i.e.  both defline and pdef for the same gene OR just one or the other.
# For a given gene, ONE description per defline and/or  ONE per 'pdef' is expected and assumed.  Otherwise, uniqueness is not enforced for other fieds (first-come-first-served)

# The deflines are assumed to be sorted on the first field

# The numeric suffix of the gene_name in the defline file is striped off, and the remainder is what's used as the ID when searching for the matching 'gene' feature in the gff file.
# This way the first defline/pdef encountered for a given gene_name is what goes into the updated gff and the rest are ignored.   


import os, getopt, sys
from os import path
import gffutils

# return strings for 1-item results, instead of list 
#gffutils.constants.always_return_list = False


featureTypes = ['gene', 'mRNA', 'CDS']  #default
suffixToAdd = None

if len(sys.argv) < 4:
    script=os.path.basename(sys.argv[0])
    print ('usage: %s -a <gff file> -d <deflines file> [-s <suffix>] [-f <feature type>]', script)
    sys.exit(2)
try: 
    opts, extras = getopt.getopt(sys.argv[1:], 'a:d:s:f:')
    for opt, arg in opts:
        if opt == '-a':
            inAnnotFile = arg
        if opt == '-d':
            deflinesFile = arg
        if opt == '-s':
            suffixToAdd = arg  # add this suffix to defline ids in order to match it with gff IDs       
        if opt == '-f':
            featureTypes = arg.split(",")
            
            
except getopt.error as err: 
    print (str(err)) 
    sys.exit(2)


def make_db(inAnnotFile):
    #create a gffutils database
    dbFName = inAnnotFile + '.db'
    if path.exists(dbFName):
        db = gffutils.FeatureDB(dbFName)
    else:
        db = gffutils.create_db(inAnnotFile, dbfn=dbFName, force=True, keep_order=True, merge_strategy='merge', sort_attribute_values=True)
    # alternatively just create a gffutils database "in-memory"
    #db = gffutils.create_db(inAnnotFile, ':memory:', force=True, keep_order=True, merge_strategy='merge', sort_attribute_values=True)

    print('Database loaded/created successfully.')
    return db

    

def parse_deflines(deflinesFile):
#create a dictionary of deflines
    
    if suffixToAdd:
        print('When looking for matching gene records, will add suffix ', suffixToAdd, ' to the gene name in deflines file')
        
    defs = {}
    defLineAdded = protDefAdded = False
    prevID = ''
    with open(deflinesFile) as f:
        for line in f:
            l = line.split('\t')
            geneID=(l[0].split("."))[:-1]  #strip off sufiix after LAST dot
            geneID='.'.join(geneID)
            if not geneID:
                 raise Exception('Incorrect data')
            if suffixToAdd:
                geneID += suffixToAdd
            if geneID != prevID:
                defLineAdded = protDefAdded = False
            # one defline and one pdef record is allowed for each gene;  the two get concatenated into a single dict value
            if (l[1] == 'defLine' and not defLineAdded ):
                val = '[defLine] ' + l[2].rstrip('\n') + '. '
                if geneID in defs:
                    defs[geneID] += val
                else: 
                    defs[geneID] = val
                defLineAdded = True
            elif ( l[1] == 'pdef' and not protDefAdded):
                val = '[pdef] ' + l[2].rstrip('\n') + '. '
                if geneID in defs:
                    defs[geneID] += val
                else: 
                    defs[geneID] = val
                protDefAdded = True
            prevID = geneID
    return defs



#MAIN
                    
db = make_db(inAnnotFile)

defs = parse_deflines(deflinesFile)


outGffFn=inAnnotFile.rstrip('gff') + 'w_deflines.gff'
with open(outGffFn, 'w') as fout:
    cnt=0
    for d in db.directives:
        fout.write('##{0}\n'.format(d))

    for feature in db.all_features():
        if feature.featuretype == 'gene':
            geneID = feature['ID'][0]
            if geneID in defs:
                #print('DBG: for ',geneID,', adding  defline: ',defs[geneID])   
                feature.attributes['Note'] = '"'+str(defs[geneID])+'"'
                cnt += 1
            #else:
                #print('No defline found for ID ', geneID)

#        dialect=gffutils.helpers.infer_dialect(feature.attributes)
#        print('Attributes (dialect: ',dialect,'):')
#        for i in sorted(feature.attributes.items()):
#            print('{i[0]}: {i[1]}'.format(i=i))
        fout.write(str(feature) + '\n')

print(cnt, ' annotations were updated. Done')










        
