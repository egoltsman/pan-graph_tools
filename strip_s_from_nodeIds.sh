#!/bin/bash

#strips leading 's' from any occurence of s[0-9] in a GFA graph. This is meant to convert minigraph's rGFA output into a form that vg tools can work with, i.e. node ids must be numeric.
#WARNING: this is not very smart!

cat $1 | sed  's|s\([0-9]\)|\1|g' > ${1}.numIds
