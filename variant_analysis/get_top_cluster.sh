#!/bin/bash
set -e

#Performs sequence similarity clustering on the input sequences (presumably indel alleles) and returns the sequences in the most abundant cluster


indelsFasta=$1
if [ "$#" -ne 1 ]; then
    echo "Illegal number of parameters"
    echo "Usage: $0 indelsFasta"
    exit
fi

if [ -z "$UTILS_PATH" ]; then
    UTILS_PATH=/global/homes/e/eugeneg/utils
fi

cdhit_bin=$UTILS_PATH/cd-hit-v4.8.1-2019-0228/cd-hit-est
seqtk_bin=$UTILS_PATH/seqtk/seqtk


outPrefix=${indelsFasta}.CD-HIT

nFastas=$(grep -c '>' $indelsFasta)
minTopClusterSize=$(echo "$nFastas / 5" | bc)   #the top cluster should contain at least 20% of the sequences to be considered. TODO: make this a command arg
echo "Min top cluster size: $minTopClusterSize"

#cluster w cd-hit
$cdhit_bin -T 12 -i $indelsFasta -o $outPrefix -c 0.8 -n 5 -sc 1 -sf 1

sed -n '/Cluster 0/,/>Cluster 1/p' ${outPrefix}.clstr | grep -v '>Cluster' > ${outPrefix}.Cluster0
cl0_size=$(wc -l < ${outPrefix}.Cluster0)

echo "Top cluster size: $cl0_size"

if [[ $cl0_size -lt $minTopClusterSize ]]; then
    echo "Top cluster size is smaller than the threshold of $minTopClusterSize."
    exit
fi

#take top cluster and extract the seqs

cat ${outPrefix}.Cluster0 | cut -f2 -d' ' | sed 's|>||g' | sed 's|\.\.\.||g' > ${outPrefix}.Cluster0.names

#cd-hit truncates names in the cluster file
for n in `cat ${outPrefix}.Cluster0.names`; do grep $n $indelsFasta | sed 's|>||g'; done >  ${outPrefix}.Cluster0.list_fixed

$seqtk_bin subseq $indelsFasta ${outPrefix}.Cluster0.list_fixed > ${outPrefix}.Cluster0.fasta

rm ${outPrefix}.Cluster0.list_fixed ${outPrefix}.Cluster0.names  ${outPrefix}.Cluster0

#msa to confirm clustering
#mafft --thread 12 --clustalout --adjustdirection ${outPrefix}.Cluster0.fasta  > ${outPrefix}.Cluster0.mafft.clw
