#!/usr/bin/env python3

"""
Loads snarl data from vg  onto the graph.  Given a graph (GFA2) and a list of snarls and snarl traversals from vg snarls (json format; use 'vg view' to convert from binary to json)),  adds the traversals as new sub-paths. The sub-path line has a unique id and is tagged with names of the parent sample-paths


Common traversal "tips" allow to assign a set of traversals to their original unique snarls, allowing to create snarl ids which we can then add as tags to the variant sub-paths:

P   7295<>7297_1   7295+,8927+,7296+,8928+,7297+  pp:Z:pBd1_1.3,pRef.2  np:i:2  sn:Z:7295<>7297   si:J:{"directed_acyclic_net_graph": true, "end": {"node_id": "7297"}, "start": {"node_id": "7295"}, "start_end_reachable": true, "type": 1}

Outputs: GFA2

Additionally, loads snarl and traversal data into a pandas data frame and exports it in a csv file.


"""

import os, getopt, sys
from os import path

try:
    import gfapy
except ModuleNotFoundError:
    sys.stderr.write("ERROR: Module 'gfapy' is not installed! Load the 'gfa+' conda environment or install gfapy on your system!")
    exit(1) 

if 'SCRIPTS_PATH' in os.environ:
    scripts_path = os.getenv("SCRIPTS_PATH")
else:
    scripts_path = '/global/homes/e/eugeneg/scripts/pangenome_graph'
sys.path.append(scripts_path)

import my_graph_ops
from my_graph_ops import *


#work with protobuf serializations
#sys.path.append('/global/homes/e/eugeneg/utils/vgteam/vgtools/')
#import vg_pb2 as vgproto

snarlsFn = None
traversalsFn = None
outGfaFn = None

if len(sys.argv) == 1 or '-h' in sys.argv[1:]:
    script=os.path.basename(__file__)
    print ("usage: %s -g <GFA2 file> -s <snarls> <-t <snarl_traversals> -o <output GFA2 file>   " %(script))
    sys.exit(2)
try: 
    opts, extras = getopt.getopt(sys.argv[1:], 'g:s:t:o:')
    for opt, arg in opts:
        if opt == '-g':
            inGfaFn = arg
        if opt == '-t':
            traversalsFn = arg
        if opt == '-s':
            snarlsFn = arg
        if opt == '-o':
            outGfaFn = arg
            
except getopt.error as err: 
    print (str(err)) 
    sys.exit(2)

if not snarlsFn or not traversalsFn:
    raise Exception('Both snarls and snarl traversals must be provided')

if not inGfaFn:
    raise Exception('A graph in GFA2 format must be provided as input')




    
def match_traversalStr_to_pathStr(pathStrings, oriSegmentsL):
    # Finds a unique parent path that contains the segements in the exact order and orienation
    # If multiple matches are found, return None
    parentPathNames = []
    for s in oriSegmentsL:
        assert(s[-1] == '+' or s[-1] == '-')
    traversalSegStr = ' '.join(oriSegmentsL)
    oriSegmentsL_rev = []
    for seg in reversed(oriSegmentsL):
        if seg[-1] == '-':
            seg = seg[:-1] + '+'
        else:
            seg = seg[:-1] + '-'
        oriSegmentsL_rev.append(seg)
    traversalSegStrRev = ' '.join(oriSegmentsL_rev)
    
    #print(f'Traversal {traversalSegStr} (or {traversalSegStrRev}) :  Looking for matching parent path ...')
    for name, pathStr in pathStrings.items():
        if traversalSegStr in pathStr  or traversalSegStrRev in pathStr:
            parentPathNames.append(name)
            #print(f'Matching path {parentPathNames[-1]} found!')
    return parentPathNames


def match_traversal_to_path(pathsDoL, traversalL):
    '''
    For each traversal, finds all parent paths that contain it in the exact order/orientation.
    Multiple matches are allowed
    '''
    parentPathNames = []
    #also check traversal in reverse orientation
    traversalL_rev = []
    for seg in reversed(traversalL):
        if seg[-1] == '-':
            seg = seg[:-1] + '+'
        else:
            seg = seg[:-1] + '-'
        traversalL_rev.append(seg)

#    travStr = ' '.join(traversalL)
#    travRevStr = ' '.join(traversalL_rev)
#    print(f'Traversal {travStr} (or {travRevStr}) :  Looking for matching parent path ...')
    for name, pathL in pathsDoL.items():
        if my_graph_ops.list_A_contained_in_B(traversalL, pathL) \
           or \
           my_graph_ops.list_A_contained_in_B(traversalL_rev, pathL):
            parentPathNames.append(name)
#            print(f'Matching path {parentPathNames[-1]} found!')

    return parentPathNames



def add_subpaths(gfa2, snarlInfo):
    #build a new GFA2 path entry from a snarl traversal,  adding snarl and traversal names as tags
    #pathStrings = my_graph_ops.all_paths_as_str(gfa2)
    allPaths = my_graph_ops.all_paths_as_DofL(gfa2) 
    n=0
    for mySnarl in snarlInfo.values():
        for traversal in mySnarl.traversals:
            oriTravSegments = []
            for visit in traversal.walk:
                orient = '-' if visit.backward == True else '+'
                oriTravSegments.append(str(visit.node_id + orient))
            parentSamplePaths = match_traversal_to_path(allPaths, oriTravSegments)
            if not len(parentSamplePaths):
                #raise Exception('traversal could not be matched with an existing path')
                continue
            segString = ' '.join(oriTravSegments)  # here we don't include edge ids between segments. This is optional in GFA2 format
            parentPathsStr = ','.join(parentSamplePaths)
            pLine = gfapy.Line('O\t' + traversal.name + '\t' + segString \
                               + '\tsn:Z:' + mySnarl.ID \
                               + '\tpp:Z:' + parentPathsStr \
                               + '\tnp:i:' + str(len(parentSamplePaths)) \
                               + '\tnd:i:' + str(traversal.is_nested)  )
            #rint(type(pLine))
            #rint(pLine)
            gfa2.add_line(pLine)
            traversal.samplePaths = parentSamplePaths.copy()
            n += 1
    print(f'Added {n} traversals as allele-paths')
            





    
    
#MAIN

print('Loading Gfa ...');
gfa=gfapy.Gfa.from_file(inGfaFn)
print('Gfa data loaded successfully.')

if gfa.version == 'gfa1':
    gfa2 = gfa.to_gfa2()
elif gfa.version == 'gfa2':
    gfa2 = gfa
else:
    raise Exception('Invalid header in gfa file; must be v 2.0 or 1.0')

        
snarlInfo = {} 

snarlInfo = my_graph_ops.vg_snarls_to_snarlInfoD(snarlsFn)
my_graph_ops.parse_vg_traversals(traversalsFn, snarlInfo)
add_subpaths(gfa2, snarlInfo)

#Load relevant data into a pandas data frame and do some basic stats, then save as csv
snDF = SnarlsDF(snarlInfo, gfa2)
snDF.debug()
trDF = TravsDF(gfa2)
trDF.debug()

if outGfaFn:
    snDF.print_to_csv(outGfaFn)
    trDF.print_to_csv(outGfaFn)
    gfa2.to_file(outGfaFn)
    
print('Done')
