#!/bin/bash -x



#Extracts the representitive sequence name  from CD-HIT output (.clstr file) and does blastn against NCBI

#e.g. the orange MITE db is in /global/dna/projectdirs/plant/assembly/eugeneg/SEQ_DATA/Plants/Citrus/33_orange_MITE_db

clusterFile=$1
mite_db=$2

cluster0Fasta="${clusterFile%.*}.Cluster0.fasta"

seqName=`grep "*" $clusterFile | head -n1 | cut -f2 -d' ' | sed 's|>||g' | sed 's|\.\.\.||g'`
grep -A1 $seqName $cluster0Fasta > $seqName.fasta

shifter --image=bryce911/blastplus:2.9.0 blastn \
	-query $seqName.fasta -db $mite_db \
	-evalue 1e-30 -perc_identity 75 -word_size 28 -task megablast -show_gis -dust yes -num_alignments 5  \
	-outfmt '6 qseqid sseqid bitscore evalue length pident qstart qend qlen sstart send slen staxids salltitles'  \
	-num_threads 16 > $seqName.vs_nr.blast_out

