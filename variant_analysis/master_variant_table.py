#!/usr/bin/env python3
"""
Builds the master table to store various metadata pertaining to each variant (bubble).  
The initial variant info is parsed out of a vg-produced vcf file. 
Once the table is created, additional data can be added to the variants by passing in
a csv file containing exactly TWO columns, with the first column containing the variant 
IDs already present in the table and the second column containing the data to add. 
"""

import getopt, sys
from os import path
from shutil import copyfile
import logging as log
import pandas as pd
import json

log.basicConfig(level=log.INFO, format="%(levelname)s: %(message)s",stream=sys.stdout)

class Variant:
    # Class to hold data and methods related to a single variant bubble. Can get variant info from either a vg-constructed vcf file entry or a minigraph-call bed entry. 

    def __init__(self, varId, refPathName, posOnRefPath, genotypes, refAlleleLen = None, altAllelesLenL = []):
        self.varId = varId
        self.refPathName = refPathName
        self.posOnRefPath = posOnRefPath
        self.genotypes = genotypes
        # optional - may or may not be present in the initial input
        self.refAlleleLen = refAlleleLen
        self.altAllelesLenL = altAllelesLenL
        
    @classmethod
    def load_line_vcf(cls, fields):
        #init from a *vg-deconstruct* vcf line, returning a parameterized constructor call
        refPathName = fields[0]
        posOnRefPath = int(fields[1])  #IMPORTANT: this is the position *on the path*, not to be confused with SO tags in rGFA which refer to global coordinates
        varId = fields[2]
        refAlleleLen = len(fields[3])
        altAllelesLenL = [ len(seq) for seq in fields[4].split(",") ]  #we don't store the sequences to save space
        genotypes = fields[9:]
        
        return cls(varId, refPathName, posOnRefPath, genotypes, refAlleleLen, altAllelesLenL)

    @classmethod
    def load_line_minigraph_bed(cls, fields):
        #init from a *minigraph-call* bed line, returning a parameterized constructor call
        return cls()  #TODO

    
    def to_dict(self):
        return {
            'varId' : self.varId,
            'refPathName' : self.refPathName,
            'posOnRefPath' : self.posOnRefPath,
            'refAlleleLen' : self.refAlleleLen,
            'altAllelesLenL' : self.altAllelesLenL,
            'genotypes' : self.genotypes
        }
    
    def get_genotypes_str(self):
        return ','.join(self.genotypes)
        
    def print_variant_brief(self):
        print (self.refPathName, self.posOnRefPath, self.get_genotypes_str())
        

        
def add_variant_to_masterDF(var, masterDF):
    """
    SLOW! only use this for small tables
    """
    if masterDF['varId'].eq(var.varId).any():
        log.warning('Variant id %s is already in the master table!', var.varId)
        log.warning('Adding variant with a non-unique id!')
    data = var.to_dict()
    return masterDF.append(data, ignore_index=True)



def check_duplicates(df):
    df_nonUniq_ids = df[df['varId'].duplicated(keep=False)]
    if df_nonUniq_ids.shape[0] > 0:
        log.warning('Duplicate variant ids found!')
        print(df_nonUniq_ids)
        return 1
    return 0


       
    
def load_masterDF_vcf(inVcfFile):
    # read vg-produced vcf file and populate the master df
    fh = open(inVcfFile)
    content = fh.readlines()
    nextLn = 0
    sampleSeqs = []
    for line in content:
        nextLn +=1
        if line.startswith("#CHROM"):
            fields=line.rstrip('\n').split("\t")
            sampleSeqs = fields[9:]
            if sampleSeqs[0][0] == 'p':
                #strip leading 'p', if present, from the path name
                sampleSeqs = [ s[1:] for s in sampleSeqs ]
            break

    if not len(sampleSeqs):
        log.error('Could not extract query sequence names from vcf file!')
        exit()

    #for efficiency, first gather all vars into a dict 
    varsD = {} 

    #initialize
    firstEntry = content[nextLn].rstrip('\n')
    firstVar = Variant.load_line_vcf(firstEntry.split("\t"))
    for k in  firstVar.to_dict().keys():
        varsD[k] = []

    #populate dict
    for line in content[nextLn:]:
        line = line.rstrip('\n')
        var = Variant.load_line_vcf(line.split("\t"))
        for k, v in  var.to_dict().items():
            varsD[k].append(v)
    fh.close()

    #create the dataframe 
    mDF = pd.DataFrame.from_dict(varsD)

    #add query sequence name columns (empty for now)
    #for s in sampleSeqs:
    #    mDF[s] = '' 
        
    return mDF




def append_masterDF_column(df,csvFile):
    '''
    Creates a new series from a 2-column file in a form  [varId\tvalue]. Appends as a column in a masterDF.
    The first column must have the header 'varId'.
    No format restrictions to to the cells. If json is detected, we attemp to parse it using Pandas read_json() function.
    '''
    
    def is_json(myjson):
        try:
            json_object = json.loads(myjson)
        except ValueError as e:
            return False
        return True
    
    nColsOrig = len(df.columns)
    new = pd.read_csv(csvFile, sep='\t', header=0)
    assert new.columns[0] == 'varId', "First column of the to-add csv must be \"varId\""
    
    #if json, parse column into a single json object and then read that into the new data frame
    if is_json(new[new.columns[1]][0]):  #2nd column is what we're interested in
        jsonsL = new[new.columns[1]].to_list()
        singleJString = '[' + ','.join(jsonsL) + ']'   # we get a JSON array
        new = pd.concat([new['varId'], pd.read_json(singleJString)], axis=1)

    print(new.head())

    
    merged = df.merge(new, on='varId', how='left')
    assert len(merged.columns) == nColsOrig + len(new.columns[1:]) 
    return merged



if __name__ == "__main__":

    inVcfFile = None
    inDFFile = None
    csvToAdd = None
    outFn = None

    
    if len(sys.argv) < 2:
        script=path.basename(sys.argv[0])
        print ('usage: %s -v <vcf file> -o <output.csv> | -d <input.csv> -c <to_add.csv> )', script)
        sys.exit(2)

    try: 
        opts, extras = getopt.getopt(sys.argv[1:], 'v:d:c:o:')
        for opt, arg in opts:
            if opt == '-v':
                inVcfFile = arg
            if opt == '-d':
                inDFFile = arg
            if opt == '-c':
                csvToAdd = arg
            if opt == '-o':
                outFn = arg
                
    except getopt.error as err: 
        log.error(err)
        sys.exit(2)

    if inVcfFile and not outFn:
        log.error("-o <output_file_name>  is required when starting with a vcf file")
        sys.exit()

        
    if csvToAdd and not inDFFile:
        log.error("-c option must be combined with -d (current table)")
        sys.exit()

    if csvToAdd:
        #back up original
        try:
            copyfile(inDFFile, inDFFile + ".BAK")
        except IOError as e:
            log.error("Unable to copy file. %s" % e)
            exit(1)
        outFn = inDFFile
    elif path.exists(outFn):
        log.warning("Specified output file exists; will overwrite!")
        if input("Continue? [y]/n ") == 'n':
            exit()

    
    
    if inDFFile:
        log.info('Will polulate the table with the values in %s', inDFFile)
        masterDF = pd.read_csv(inDFFile, sep='\t', index_col=0)
        if csvToAdd:
            masterDF = append_masterDF_column(masterDF,csvToAdd)
    elif inVcfFile:
        masterDF = load_masterDF_vcf(inVcfFile)

    if masterDF.shape[0] == 0:
        log.error('No indels could be extracted from the input!')
        sys.exit()

    
    if check_duplicates(masterDF):
        log.info('Will remove duplicate variant entries (by varId), keeping only the first occurence.')
        masterDF = masterDF.drop_duplicates(subset=['varId'], keep='first')

    masterDF.to_csv(outFn, sep='\t') 

    print('Constructed and saved a table of shape ', masterDF.shape)
    


        
