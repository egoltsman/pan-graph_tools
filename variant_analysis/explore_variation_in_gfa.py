#!/usr/bin/env python3
"""


#TODO: split this into two programs!


#### APP 1:
A graph can be populated apriori with feature annotations (as Fragments + tags) and snarl information (as traversal-paths + tags), which is only possible in GFA2 format.
Given such a graph, builds a map of {snarl} -> [affected_features]   and a reciprocal {feature} -> [snarl(s)]

Global (Phytozome) gene ids (without transcipt extensions) are taken from the Fragment id field, and the deflines are extracted from the nt:Z: tag on the F lines
Snarl ids are parsed out of the sp:Z tag on each variant sub-path 

First, every *gene function* is tallied up (based on defline identity) so that later any individual 'gene' feature that's found to be associated with a snarl-path can be reported along with its *count* across the entire sample-path(s)
This helps distinguish CNVs from presense/absense variations.   


### APP 2:

Report a summary of segment conservation in snarls (snarls traversals are considered without the "tips" segments)

Total sequence in all paths combined
Total sequence in all snarls 

For each accession:
  Total sequence in all paths combined
  Total sequence in snarl traversals matching a path from this sample
  Breakdown of snarl traversals by weight:
     e.g.  w 1: 20%, w 2: 40%, w 3: 40%
     Report by bp and by n snarl-travs
"""



import os, getopt, sys
from os import path
import pandas as pd

try:
    import gfapy
except ModuleNotFoundError:
    sys.stderr.write("ERROR: Module 'gfapy' is not installed! Load the 'gfa+' conda environment or install gfapy on your system!")
    exit(1)

try:
    import matplotlib.pyplot as plt
except ModuleNotFoundError:
    sys.stderr.write("ERROR: Module 'matplotlib' is not installed! Load the 'gfa+' conda environment or install gfapy on your system!")
    exit(1)
    



sys.path.append('/global/homes/e/eugeneg/scripts/pangenome_graph')
import my_graph_ops
#from my_graph_ops import *




traversalsCSV = None
snarlsCSV = None
queryFeatureID = None
runReport = False


if len(sys.argv) < 8 or '-h' in sys.argv[1:]:
    script=os.path.basename(__file__)
    print ("usage: %s -g <GFAv2_file> -s <snarls_csv> -t <traversals_csv> -r <reference_sample_name> << -F [feature ID] -R [report_file_prefix] >>" %(script))
    sys.exit(2)
try: 
    opts, extras = getopt.getopt(sys.argv[1:], 'g:s:t:r:F:R:')
    for opt, arg in opts:
        if opt == '-t':
            traversalsCSV = arg
        if opt == '-s':
            snarlsCSV = arg
        if opt == '-g':
            gfaFn = arg
        if opt == '-r':
            refSample = arg
        if opt == '-F':
            featureID = arg
        if opt == '-R':
            runReport = True
            outPrefix = arg

except getopt.error as err: 
    print (str(err)) 
    sys.exit(2)

if not (traversalsCSV and snarlsCSV and gfaFn ):
    raise Exception('Not all required data was provided')


gfa=gfapy.Gfa.from_file(gfaFn)
assert(gfa.version == 'gfa2')

snarlsDF = pd.read_csv(snarlsCSV, index_col=0)
travsDF = TravsDF(traversalsCSV).get_df()
print(travsDF.head(3))


def get_variable_features():

    #couln't figure out how to split and match on the dataframe directly, do doing it the ugly way here
    bubbleNodes = {}
    #bubbles with no nodes are legal;  replacing NaN with ''
    snarlsDF.bubble_nodes = snarlsDF.bubble_nodes.fillna('')

    for i,r in snarlsDF.iterrows():
        bubbleNodes[ r['snName'] ] = r['bubble_nodes'].split(':')
        
    for frag in gfa.fragments:
        #testing if the fieature-fragment is inside a bubble
        for k, nodes in bubbleNodes.items():
            if frag.sid.name in nodes:
                print('Frag ', frag, ' matches a node in bubble ',k)
    
        #Feature-fragments can also match bubble "tips"
        #If the feature matches a bubble "tip", it has to extend in the direction of the bubble to be considered affected by it.
        matchesLTipDF = snarlsDF[snarlsDF['b-tip_L'].astype(str) == frag.sid.name]
        if (not matchesLTipDF.empty)  and  frag.s_end == frag.sid.length - 1:
            print('Frag ', frag, ' matches tip_L in bubble ', matchesLTipDF.snName) 
        matchesRTipDF = snarlsDF[snarlsDF['b-tip_R'].astype(str) == frag.sid.name]
        if (not matchesRTipDF.empty)  and  frag.s_beg == 0:
            print('Frag ', frag, ' matches tip_R in bubble  ', matchesLTipDF.snName) 
                

def run_variation_report(outPrefix,nBins=4):

    outFn = outPrefix + '.REPORT'
    outPng = outPrefix + '.png'
    
    #collect global info

    from collections import OrderedDict  #we want to preserve order of keys as they're entered
    globalStats = OrderedDict()
    
    accessionsL = my_graph_ops.get_accessions(gfa)
    assert(len(accessionsL))
    globalStats['totAccessions'] = len(accessionsL)
    globalStats['totalSeqAllPaths'] = 0
    for p in gfa.paths:
        if p.name[0] != 'p':  #only considering canonical sample-paths (assumed to begin with 'p')
            continue
        for oriSeg in p.captured_segments:
            globalStats['totalSeqAllPaths'] += oriSeg.LN
            
    globalStats['totalBubbles'] = snarlsDF['snName'].count()
    globalStats['totalNestedBubbles'] = snarlsDF['parent_snarl'].count()
    globalStats['maxBubbleSize'] = snarlsDF['bubble_size'].max()

    sumAllAlleles = 0
    maxAlleleSize = 0
    for accn in accessionsL:
        sumAllAlleles += travsDF[accn].sum();
        if travsDF[accn].max() > maxAlleleSize:
            maxAlleleSize = travsDF[accn].max()
            
    globalStats['totalSeqAllAlleles'] = sumAllAlleles
    globalStats['maxAlleleSize'] = maxAlleleSize
    pctInAlleles = sumAllAlleles /  globalStats['totalSeqAllPaths']
    globalStats['pctInAlleles'] = round(pctInAlleles, 2)
    
    globalStats['maxAllellesInABubble'] = snarlsDF['nTraversals'].max()
    globalStats['maxPathsThroughABubble'] = snarlsDF['nPaths'].max()

    with open(outFn, "w") as fout:
        for k, v in globalStats.items():
            fout.write(str(k) + ' : ' + str(v) + '\n') 


        

    
    # Collect global info per accession, including binned breakdown of allele counts
    # For easier parsing and slicing downstream we store this in a pandas df

    #allele weights are expressed as decimals, so we break that up into bins
    binDefinitions = get_bins(nBins,1.0) 
    reportDF = pd.DataFrame()
    colsToPlot = []
    
    for accn in accessionsL:   #TODO: parallelize this!
        totalSeqInSamplePaths = 0  #FIX ME: not all of the sample may be present in paths, and some paths may overlap leading to inflated size
        for p in gfa.paths:
            if p.name[0] != 'p':
                continue
            if accn not in p.name:
                continue
            for oriSeg in p.captured_segments:
                 totalSeqInSamplePaths += oriSeg.LN
        data = {
            'Accession' : accn,
            'bp_in_paths' : totalSeqInSamplePaths,
            #TODO:  Add paths_redundant_len
            'n_alleles_TOT' : travsDF[accn].count(),
            'bp_in_alleles_TOT' : travsDF[accn].sum(),
            '%bp_in_alleles_TOT' : travsDF[accn].sum() / totalSeqInSamplePaths
        }

        myTravsDF = travsDF[travsDF['travPathNames'].str.contains(accn)]
        #Note: the same traversal (allele) can match more than one accession (path), or even match the same accession multiple times if some paths were mis-aligned due to repeats
        myNonRefTravsDF = myTravsDF[myTravsDF[accn] != myTravsDF[refSample]] 
        data['n_alleles_Non-Ref'] = myNonRefTravsDF[accn].count()


        ### parse into weight-bins

        for bin in binDefinitions:
            binNameStr = '-'.join([str(x) for x in  bin])
            binDF = myTravsDF[ (myTravsDF['alleleWeight'] > bin[0]) & (myTravsDF['alleleWeight'] <= bin[1]) ]

            if not binDF.empty:
                binTravsOutFN = accn + binNameStr + '.traversals.fasta'
                write_bin_seqs(binTravsOutFN, binDF)

            
            # colName = 'n_allels_wt_' + binNameStr  
            # data[colName] = binDF[accn].count()
            
            # colName = 'bp_allels_wt_'+ binNameStr
            # data[colName] = binDF[accn].sum()  

            colName = '%_allels_wt_'+ binNameStr
            if colName not in colsToPlot:
                colsToPlot.append(colName)  # plot it later
            if binDF.empty:
                data[colName] = 0.0
            else:
                data[colName] = binDF[accn].count() / myTravsDF[accn].count()  
            
            colName = '%bp_in_allels_wt_'+ binNameStr
            if binDF.empty:
                data[colName] = 0.0
            else:
                data[colName] = binDF[accn].sum() / myTravsDF[accn].sum()
                
        reportDF = reportDF.append(data, ignore_index=True)


    #first, round everthyng to two decimals
    reportDF = reportDF.round(2) 

    #now turn selected cols to int
    for col in ['bp_in_paths', 'n_alleles_TOT', 'n_alleles_Non-Ref', 'bp_in_alleles_TOT']:
        reportDF[col] = reportDF[col].astype(int)



    toPlotDF = pd.DataFrame(accessionsL, columns=['Accession'])
    for col in colsToPlot:
        print(reportDF[col])
        toPlotDF = toPlotDF.join(reportDF[col])

    toPlotDF.plot(kind='bar', stacked=True, x='Accession', title="% of alleles in weight category")
    plt.savefig(outPng)
    print('Plots saved to ', outPng)

        
    reportDF.set_index('Accession', inplace=True)
    reportDF.to_csv(outFn, sep='\t', mode='a')
    print('Accession-based report written to ', outFn)


    
    
    
def feature_query(featureID):
    pass

def write_bin_seqs(outFN, binDF):
    fout = open(outFN, "w")
    for travName in binDF['travName'].tolist():
        if gfa.line(travName):
            travSeq = path_to_seq(travName,gfa)
            fout.write(">%s\n%s\n" % (travName, travSeq))
        else:
            print('WARNING: traversal not registered as a path in the gfa file, so I cant get the sequence for it: ', travName)
    fout.close()


#MAIN

if runReport:
    run_variation_report(outPrefix)

#get_variable_features()    
