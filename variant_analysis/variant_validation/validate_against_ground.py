#!/usr/bin/env python

#Extracts and sorts numeric values from user-specified column of the input query and "ground truth" file in bed format.
#Calculates Precision and Recall

import os, getopt, sys
import numpy as np

assert sys.version_info >= (3, 4), "Python version >= 3.4 is required to run this code" 

column = 1
roundDownToNear = 10
mergeSameSampleNeighbors = 0
postionsRange = None

if len(sys.argv) < 6:
    script=os.path.basename(sys.argv[0])
    print ('usage: %s -q <test file> -t <truth file> -c <column>  << -r [pad_to_n_bp] -p [startPos-endPos] -M (merge variants with common seq name?) >>' % script)
    sys.exit(2)
try: 
    opts, extras = getopt.getopt(sys.argv[1:], 'q:t:c:p:r:M')
    for opt, arg in opts:
        if opt == '-q':
            queryFile = arg
        if opt == '-t':
            truthFile = arg
        if opt == '-c':
            column = int(arg)
        if opt == '-p':
            postionsRange = arg
        if opt == '-r':
            roundDownToNear = int(arg)  # Round called variant positions to nearest n.  For example, setting this to 1000 will consider test positions 104567 and 104321 to be within the allowed error padding and retur a true positive.
        if opt == '-M':
            mergeSameSampleNeighbors = 1


except getopt.error as err: 
    print (str(err)) 
    sys.exit(2)



startcolIdx=column-1
endcolIdx=startcolIdx+1

qarr=np.loadtxt(queryFile, comments='#', usecols=startcolIdx)

qarr = np.sort(qarr)
if not qarr.size:
     print('ERROR: could not parse any positions out of the query file')
     exit()

if roundDownToNear:
    decimals = np.log10(roundDownToNear)
    print('DECIMALS: ', decimals)
    if not decimals.is_integer():
        raise Exception('Round value must be a  power of 10 ')


if mergeSameSampleNeighbors:

    '''
    merge *consecutive* positions of variants belonging to the same sequence. This is only useful when the original sequence names are 
    intentionaly changed to reflect a certain desired grouping.  For example, a region on sequence foo_Chr1 spanning positions, say, 10000-20000,
    could have 5 different indel calls, each appearing on a separate row of the bed file. The sequence name for these calls can be modified
    to something like foo_Chr1.regA. Then, all the variant calls with that sequence name would be counted as a single combined variant when 
    validating against the "truth" set.  The range of the variant would start from the start of the first call in the list to the end of the last one,
    and any overlap/proximity to a variant call in the truth set would qualify as a true positive.

    IMPORTANT!! This assumes the query bed file is sorted by sequence name AND by the variant start position.  No explicit checks are performed to
    assert this.

    '''

    truarr=np.loadtxt(truthFile,comments='#', delimiter='\t', usecols=[0,startcolIdx,endcolIdx], ndmin=2, dtype='str' )
    truMergedInBlocks=np.empty((0,3), dtype='int')
    prevSeqOrigin = ''
    i = 0
    for reg in truarr:
        print('reg[0]: ',reg[0],' prev: ', prevSeqOrigin)
        if reg[0] != prevSeqOrigin:
            #new block
            truMergedInBlocks = np.append(truMergedInBlocks, [reg], axis=0)
            print('STaRT NEW: ',truMergedInBlocks[-1])
        else:
            #extend current block
            truMergedInBlocks[-1,endcolIdx] = truarr[i,endcolIdx]
            print('   ExTEND CURRENT: ', truMergedInBlocks[-1])
        prevSeqOrigin = reg[0]
        i=i+1

    truarr = np.sort(truMergedInBlocks, axis=0)
    #don't need the source name column anymore; delete to avoid mixed types in the array
    truarr = np.delete(truarr, 0, 1)
else:
    truarr = np.genfromtxt(truthFile,comments='#', delimiter='\t', usecols=(startcolIdx,endcolIdx))

truarr = truarr.astype('int')


if postionsRange:
    myMin,myMax = (int(x) for x in postionsRange.split('-'))
    if myMin:
        truarr = truarr[truarr[:,0] >= myMin, :]
    if myMax:
        truarr = truarr[truarr[:,0] <= myMax, :]


#truarr = np.sort(truarr)
truarr = truarr[truarr[:,0].argsort()]

#print(type(truarr), truarr.shape)
#print(truarr)
if not truarr.size:
     print('ERROR: could not parse any positions out of the ground truth file')
     exit()

#round values
if roundDownToNear > 1:
    decimalsToRound = int(-1*np.log10(roundDownToNear))
    sys.stderr.write('Will round the variant position to the nearest %d\n' % roundDownToNear)
    qarr = np.round(qarr, decimalsToRound)
    truarr = np.round(truarr, decimalsToRound)


qarr = qarr[:,None]  #convert qarr to a 2d array to match the shape of truarr

#print(qarr[:10,:])
#print(truarr[:50,:50])

#Inversions can be  produced/reported at the reference position that corresponds to EITHER "start" or "end" position in the truth list;
#truarr holds both start and end positions, and we consider a query element a true-positive if it matches either start or end in truarr
#Likewise,  false negatives are positions in the true set where NEITHER start nor end have a match in the query

tpBool = np.in1d(qarr,truarr, assume_unique=True)
tp=qarr[tpBool]

fpBool = ~tpBool
fp=qarr[fpBool]

#true start positions that are missing from the query
fnStartBool = np.in1d(truarr[:,0], qarr, assume_unique=True, invert=True)
#print(fnStartBool)

#true end positions that are missing from the query
fnStopBool = np.in1d(truarr[:,1], qarr, assume_unique=True, invert=True)
#print(fnStopBool)

#logical union
fnCombinedBool = np.logical_and(fnStartBool,fnStopBool)
#print(fnCombinedBool)

#TODO: as an option, add logical_or() to call false negatives based on at least one bubble end not matching the true coordinate (watch out for inversions)


nTP = tp.size
nFP = fp.size
nFN=np.count_nonzero(fnCombinedBool)
print('TP: ',nTP)
#print(tp)
print('FP: ',nFP)
print(fp)
print('FN: ',nFN)
print(truarr[fnCombinedBool])

prec = nTP/(nTP+nFP)
recall = nTP/(nTP+nFN)

print('Precision: ',prec)
print('Recall: ',recall)

sys.stderr.write('Done.\n')
