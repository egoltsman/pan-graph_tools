#!/bin/bash
set -e

if [ "$#" -lt 3 ]; then
    echo -e "This is a wrapper script that prepares the query variant positions from 'vg deconstruct' (!) and the ground-truth positions from Visor (!)\n for validation with validate_against_groud.py. Produces two bed fies and feeds them into the program.\n"

    echo -e "IMPORTANT!! The variant positions that are reported by vg are positions on a given *path*, which may not reflect the actual coordinate\non the original reference sequence!  Users must ensure that proper offsets are applied to the vcf file to account for this prior to runing this script!\n\n"

    echo "Usage: run_validation_on_vg.sh [vg_vcf] [truth_bed] [pad_to_n_bp]"
    exit;
fi

vgVcf=$1
truthBed=$2
pad_to=$3

if ! [[ $pad_to =~ ^[0-9]+$ ]] ; then
   echo "error: the padding parameter must be an integer" >&2; exit 1
fi

if [ -z "$SCRIPTS_PATH" ]; then
    SCRIPTS_PATH=/global/homes/e/eugeneg/scripts/pangenome_graph
fi


validator=${SCRIPTS_PATH}/variant_analysis/variant_validation/validate_against_ground.py

which $validator || exit

cat $vgVcf  | awk '/^#/ {next}; $10>0 {print $1,$2,$8}' > vg-vars_called.bed

#Copy-paste variants do not affect the var position stated in the "truth" file. Instead they create an insertion which is recored in col 5.
#Similarily, cut-paste variants are only a single event in the truth file but show up as two bubbles in the graph.

#I fix this for the copy-paste TLs by adding only the *target* position to the truth set and excluding the source pos.
#For cut-paste TLs I include both the original source (i.e. del) and the target (ins)

grep -v "translocation copy-paste" $truthBed > final_truth.bed   #original copy-paste positions excluded

grep "translocation copy-paste" $truthBed > translocations
grep "translocation cut-paste" $truthBed >> translocations
while read l; do
    seqName=`echo $l | cut -f1 -d' '`
    pos=`echo $l | cut -f5 | cut -f3 -d':'`
    size=`echo $l | awk '{print $3-$2}'`
    echo -e "$seqName\t$pos\t$(($pos+$size))"
done < translocations  >> final_truth.bed
    
validator_cmd="$validator -q vg-vars_called.bed -t final_truth.bed -r $pad_to -c 2 "

$validator_cmd > validation.padded-to-${pad_to}.out


