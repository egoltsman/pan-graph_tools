#! /bin/bash


# Call structural variants in the graph using vg tools.  First, detects snarls (bubbles), then assigns "alleles" (bubble traversals) to the specified reference and query paths.
# Outputs vcf

set -e

if [ "$#" -lt 3 ]; then
    echo "Call structural variants on the graph using vg tools."
    echo "Usage: call_variants_on_GFA.sh  [input GFA] [ref path name] [query path name(s)]"
    echo "IMPORTANT: use prefixes to group multiple paths belonging to the same sample/chromosome/contig into a single vcf column. Separate by commas (no spaces!)"
    exit
fi

if [ ! -x $(which vg)  ] ; then
    echo "vg was not found!  Please install vg and add it to your PATH environment."
    exit
fi

input=$1  #GFA1 file ***WITH PATHS***
refPathName=$2    # The path name to use as the reference in the variant calling.  Can be a prefix, e.g. 'pMandarinChr1' (as oppposed to a full path name pMandarinChr1.1)
samplePathNames=$3   # The query sequence path names. Use prefixes to group multiple paths belonging to the same sample/chromosome/contig. Separate by commas (no spaces!)
# e.g. pLemonChr1,pPomeloChr1

threads=12

if [ ! $(grep -Pc '^P\t' $input) ]; then
    echo "No paths were found in the input GFA file!"
    exit
fi

#check for non-numeric ids
if [ -f ${input}.numIds ]; then
    input=${input}.numIds
else
    firstSedName=`head -n2 $input | tail -1 | cut -f2`
    if [[ $firstSedName != [0-9]* ]]
    then
	echo "converting non-numeric segment names to numeric ids  (as required by vg)..."
	cat $input | sed  's|s\([0-9]\)|\1|g' > ${input}.numIds
	input=${input}.numIds
    fi
fi

echo "building vg graph and index.."
vg convert -t 12 -p -g ${input} > ${input}.pvg
vg index -t 12 -x ${input}.xg ${input}.pvg

#!!! after coverting to packed vg format, vg index can sometimes crap out citing long paths

#A workaound suggested by glenhickey is to use "hashGraph" format instead of PackedGraph
#vg convert -ga $input > ${input}.vg
#vg index -x ${input}.xg ${input}.vg

#call bubbles (aka "snarls");  -r is optional

echo "finding snarls..."
vg snarls -t $threads -r ${input}.snarls.traversals -e -a ${input}.xg > ${input}.snarls.pb

#optionally, convert binary snarls and traversals files to txt
vg view -E ${input}.snarls.traversals > ${input}.snarls.traversals.txt
vg view -R ${input}.snarls.pb > ${input}.snarls.pb.txt


#format the command

IFS=', ' read -r -a smpArray <<< "$samplePathNames"
samplePathsArgs=''
for n in  "${smpArray[@]}"
do
    samplePathsArgs+=" -A $n"
done
    
echo "calling variants..."
vg deconstruct -t $threads -r ${input}.snarls.pb -a -e -P $refPathName $samplePathsArgs ${input}.xg > ${input}.vcf 

echo "All done."
