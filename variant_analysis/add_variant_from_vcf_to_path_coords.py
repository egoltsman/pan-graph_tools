#!/usr/bin/env python3
#Adds annotations from a vcf file as Fragments to graph Segments that appear in the paths traced by the specified sample.
# outputs GFA2

import os, getopt, sys
from os import path
import gfapy

if 'SCRIPTS_PATH' in os.environ:
    scripts_path = os.getenv("SCRIPTS_PATH")
else:
    scripts_path = '/global/homes/e/eugeneg/scripts/pangenome_graph'
sys.path.append(scripts_path)

import my_graph_ops as ops



class Variant:
    # Class to hold data and methods related to a single variant position in the vcf
    def load_data(self, fields):
        self.refName = fields[0]
        self.pos = int(fields[1])  #IMPORTANT: this is the position *on the path*, not to be confused with SO tags in rGFA which refer to global coordinates
        self.varId = fields[2]
        self.refSeq = fields[3]
        self.altSeqs = fields[4].split(",")
        self.genotypes = fields[9:]

    def get_genotypes(self):
        return ','.join(self.genotypes)
        
    def print_variant_info(self):
        print (self.refName, self.pos, self.genotypes)
        
    def get_ref_allele_len(self):
        return len(self.refSeq)

    def get_alt_alleles_lens(self):
        alt_lengths=[]
        for alt in self.altSeqs:
            alt_lengths.append(str(len(alt)))
        alt_lenghths_str = ','.join(alt_lengths)
        return alt_lenghths_str


    
def add_vars_as_fragments(indels, path, gfa2):
    # Matches indel positions with Segements on the sample-path and adds them as Fragments.
    # Here it's assumed that the variants have veen deduced from the graph, and therefore, each variant must correspond exactly to the 0th position on the Segment


    #mySampleName = path.name[1:]  #strip 'p'
    
    for indelPos in indels:
        #        print(indels[indelPos].print_variant_info())
        #print('Looking for the segment matching variant pos ', indelPos)
        
        posOnPath = 0
        pathIdx = 0
        for my_s in path.captured_segments:
            segLen = int(my_s.LN)
            #            print(my_s.sid, posOnPath, segLen)
            if posOnPath  > indelPos:
                #print('\t[catch] Reached segment with starting position larger than indel; will not look any further')
                break
            elif posOnPath + segLen >= indelPos: 
                # place fragment tag at the end of the segment that immediately precedes the variant position
                fragStartOnSeg =  segLen-1
                fragEndOnSeg = segLen
                startOnFrag = 0
                endOnFrag = 1

                name_ori = "var:"+str(indelPos) + '+'
                lineL = ['F', my_s.sid, name_ori, fragStartOnSeg, fragEndOnSeg, startOnFrag, endOnFrag, '*']
                fragLineStr = '\t'.join(map(str,lineL))
                #print('DBG: ',fragLineStr)
                fragLine = gfapy.Line(fragLineStr, vlevel=1)

                # dd custom tags to store variant info
                fragLine.GT = indels[indelPos].get_genotypes()
                assert (isinstance(fragLine.GT, str)), "Values of variant tags must be strings"

                fragLine.RA = indels[indelPos].get_ref_allele_len()
                fragLine.AA = indels[indelPos].get_alt_alleles_lens()
                gfa2.add_line(fragLine)

                if annotateAlleles:
                    # annotate reference allele path which could be multi-segment 
                    pathAhead = path.captured_segments[pathIdx+1:]
                    refAlleleSegments = trace_ref_allele(pathAhead, indels[indelPos])
                    if len(refAlleleSegments) > 2:
                        varSegementsStr = ''
                        for s in refAlleleSegments:
                            varSegementsStr += s.name
                            print('Complex variant of ', len(refAlleleSegments),' segments: ', varSegementsStr)

                            # extract subgraph containing the variant paths
                            bubbleTips = [None] * 2
                            bubbleTips[0] = path.captured_segments[pathIdx].name
                            bubbleTips[1] = path.captured_segments[pathIdx + len(refAlleleSegments) + 1].name
                            bubbleSubraph = ops.BubbleOperations().extract_bubble_subgraph(gfa, bubbleTips, 'gfa2')
                            
                            thisBubbleGfaName='complex_bubble.'+ str(pathIdx) + 'GFA' 
                            bubbleSubraph.to_file(thisBubbleGfaName)
                    
                    refAlleleId = "refAllelle:"+str(indelPos)   #for lack of a better way to name an allele
                    mark_ref_allele(refAlleleSegments, refAlleleId)

                break
            
            posOnPath += segLen
            pathIdx += 1
    return True


def trace_ref_allele(pathAhead, variant):
    #follow path ahead the distance of the reference allele;  Return number of segements covered by allele
    dist = 0
    refAlleleSegments = []
    allele_len = variant.get_ref_allele_len()
    for seg in pathAhead:
        if dist + int(seg.LN) < allele_len:
            dist += int(seg.LN)
            refAlleleSegments.append(seg)
            continue
        return refAlleleSegments
            
def mark_ref_allele(segments, name_prefix):
    # add a Fragment annotation for every segement of the referencd allele path
    i = 1
    for seg in segments:
        name_ori = name_prefix + '.' + str(i) + '+'
        lineL = ['F', seg.sid, name_ori, 0, int(seg.LN), 0, 1, '*']
        fragLineStr = '\t'.join(map(str,lineL))
        fragLine = gfapy.Line(fragLineStr, vlevel=1)
        i += 1
        gfa2.add_line(fragLine)
        

annotateAlleles=0
        
if len(sys.argv) < 8:
    script=os.path.basename(sys.argv[0])
    print ('usage: %s -v <vcf file> -g <input GFA1 file> -p <path> -o <output GFA2 file> <<-A(nnotate_alleles?)', script)
    sys.exit(2)

    
try: 
    opts, extras = getopt.getopt(sys.argv[1:], 'g:p:o:v:A')
    for opt, arg in opts:
        if opt == '-v':
            inVcfFile = arg
        if opt == '-g':
            inGfaFile = arg
        if opt == '-p':
            myPathName = arg
        if opt == '-o':
            outGfaFile = arg
        if opt == '-A':
            annotateAlleles = 1
            
            
except getopt.error as err: 
    print (str(err)) 
    sys.exit(2)


#parse variant info
samples = []
indelsHash = {}
for line in open(inVcfFile).readlines():
    if line.startswith("##"):
        continue
    line = line.rstrip('\n')
    fields=line.split("\t")
    if line.startswith("#CHROM"):
        samples = fields[9:]
        continue

    elif fields[0] == myPathName:
        var = Variant()
        var.load_data(fields)
        if var.pos not in indelsHash:
            indelsHash[var.pos] = var
        else :
            print('Variant position ', var.pos, ' has already been encountered!')
            # TODO: put in logic to deal with conflicting variant calls

if len(indelsHash) == 0:
    print('Error: no indels were parsed out of the input file! Bailing out.')
    sys.exit()
    
print('parsed out ', len(indelsHash), ' indels')
    
# create a gfa object
gfa=gfapy.Gfa(version='gfa1')
gfa=gfapy.Gfa.from_file(inGfaFile)

# convert to gfa2 since Fragments are only supported in that format
gfa2 = gfa.to_gfa2()        

if gfa.header.nn == None:
    gfa.add_line("H\tVN:Z:1.0")

for p in gfa.paths:
    if (p.name == myPathName):
        add_vars_as_fragments(indelsHash, p, gfa2)

gfa2.to_file(outGfaFile)

print('Done.')
    
