#!/bin/bash

set -e

#add SVTYPE and SVLEN records
#!!!! SVLEN is taken as the difference between the reference and the *first* alternate allele

vcf_in=$1

cat $vcf_in   | awk 'BEGIN {OFS="\t"; print "varId","SVtype","SVlen"} /^#/ {next} {altEnd=index($5,",")-1; alt=(altEnd == -1 ? $5 : substr($5,0,altEnd)); svlen=length(alt)-length($4); svtype = svlen < 0 ? "DEL" : "INS"}{print $3,svtype,svlen}'
