#!/usr/bin/perl
=doc
This program was originally obtained from https://github.com/abrahamdsl/vcf_to_gff3 and then
modified by Eugene Goltsman (egoltsman@lbl.gov) for use with vg-produced vcfs ONLY !


Copyright (C) 2014  Abraham Darius S. Llave under the C4 Rice Project, International Rice Research Institute

=cut

use strict;
use warnings;
use warnings FATAL => qw[uninitialized];
use Getopt::Long;
#-----------------------------------------------------------------------------
#----------------------------------- MAIN ------------------------------------
#-----------------------------------------------------------------------------
# specify your source
my $sourceName = "";
# used for identifying the feature's source as can be gleamed from the feature ID/name
#my $featureIDStart = "";
# this would be part of the feature ID/name to identify the dataset.
#my $datasetTag = "";
#my $countSNP = 0;
#my $countINDEL = 0;
my $debugMode = 0;
#my $noUnderscore = 0;
#my $noReadDepth;
#my $separator = "_";
#my $simpleFeatureName;
my $usage = "

Synopsis:

vcf_to_gff3.pl [options] file1.vcf [file2.vcf...]



Options:
 --help          Optional. Display this help.
 --path          Optional. Default directory to write results. Defaults to current/`pwd`.
 --orgsource    Optional. A string describing the organization/institute that is source of the features, for column 2 of GFF file. Defaults to a dot.

Description:

This script will convert VCF files to GFF3.
The vcf file is assumed to have been produced by 'vg deconstruct', i.e. it doesn't include any allele frequency and/or depth info and just lists structural variants at their reference genome (or path)  positions.
Any \"genotype\" info stored in the query sample columns will be transferred to the Note attribute of the 9th column in the gff.
";


my ($help, $build, $append, $path);

my $opt_success = GetOptions('help'    => \$help,
#                             'nounderscore' => \$noUnderscore,
#                             'no-depth' => \$noReadDepth,
                             'orgsource=s' => \$sourceName,
#                             'orgsource2=s' => \$featureIDStart,
		             'path=s'  => \$path,
#                             'string_tag=s' => \$datasetTag,
#                             'simple' => \$simpleFeatureName
);

die $usage if $help || ! $opt_success;

$path ||= './';
#$featureIDStart ||= "loc";
$sourceName ||= ".";
#if ($noUnderscore) { $separator = "" };

handle_message('FATAL', 'directory_does_not_exist', $path)
    unless -e $path;
handle_message('FATAL', 'path_is_not_a_directory', $path)
    unless -d $path;
handle_message('FATAL', 'directory_is_not_writable', $path)
    unless -w $path;
#handle_message('FATAL', 'string_tag should be at least 1 char', $datasetTag)
#    unless ( length($datasetTag) > 0 );
#handle_message('NOTICE', 'string_tag ', $datasetTag);
my @vcf_files = @ARGV;

if (! @vcf_files) {
  print $usage;
  handle_message('FATAL', 'no_vcf_files_to_convert');
}

for my $vcf_file (@vcf_files) {
  if ( $vcf_file !~ /\./ ){
    handle_message('FATAL', 'VCF file should have an extension name (i.e. dot)');
  }

  ( my $vcf_file_withoutExt = $vcf_file ) =~ s/\.\w+$//;
  my $gff_file = $vcf_file_withoutExt . ".gff";
  handle_message('FATAL', 'file_does_not_exist', $vcf_file) unless -e $vcf_file;
  open(my $IN, '<', $vcf_file) or
    handle_message('FATAL', 'cant_open_file_for_reading', $vcf_file);
  open(my $OUThandle, ">", $gff_file) or
    handle_message('FATAL', 'cant_open_file_for_writing', $gff_file);
   print $OUThandle "##gff-version 3\n";
   print $OUThandle "# Generated " . localtime() . " by converting from a vg-produced strucural variant vcf file \n";
   

=debug
  # a [dot] llave [at] irri.org THIS MIGHT NOT BE NEEDED IN OUR CASE
  my $header;
  while (my $line = <$IN>) {
    if ($line =~ /^#CHROM/) {
      $header = $line;
      last;
    }
  }

  handle_message('FATAL', 'no_header_line_found',
		 'File was read, but no header line was found')
    unless $header;
  my @ids = split /\s/, $header;
  splice(@ids, 0, 9);
=cut
  
  handle_message( 'NOTICE', 'Processing file', $vcf_file );
  handle_message( 'NOTICE', 'Writing to file', $gff_file );

  my @header;
  my @sampleNames;
  
  while (my $line = <$IN>) {
      chomp($line);
    if( $line =~ /^#CHROM/){
	@header = split /\t/, $line;
	@sampleNames = @header[9..$#header];
	next;
    }
    if( $line =~ /^#/ or $line =~ /^super/ or ( length( $line ) < 2 ) ){ next; }
    my @data = split /\t/, $line;
    #CHROM POS ID REF ALT QUAL FILTER INFO FORMAT SAMPLE1 SAMPLE2 ..etc
    my %record;

    @record{qw(chrom pos id ref alt qual filter info format)} =  splice(@data, 0, 9);

    my @sampleData = @data;  #leftover columns containing query-specific genotype data

    
      
    for(my $i = 0; $i < scalar(@sampleNames); $i++){
	#remove any white spaces and encode the commas with url code  (gff3 requirement for the "attributes" column)
	$sampleData[$i] =~ s/ //g;
	$sampleData[$i] =~ s/,/%2C/g;
	
	$record{'samples'}->{$sampleNames[$i]} = $sampleData[$i];
    }

    
    my @feature_ids = split /;/, $record{id}; # more often than not this is just '.'
    my $feature_id = shift @feature_ids;
    my $seqid      = $record{chrom};
    my $source;
    my $type;
    my $start      = $record{pos};
    my $end        = $start;       
    my $score      = $record{qual};

    # In VCF, variants are always in forward representation.
    # Citation: http://www.1000genomes.org/faq/what-strand-are-variants-your-vcf-file
    my $strand     = '+';                     
    my $phase      = '.';                     # not needed to specify.
    my $note;
    my $featureID;
    my $reference_seq = uc $record{ref};
 
    my @infos = split( /;/, $record{info} );
    my %sampleData = %{$record{'samples'}};   
    # Which details in the INFO field do you want to include?
    #    my @wanted = ( $noReadDepth ) ?  qw(AF) : qw(AF DP);
    my @wanted = qw(AC AF AN LV NS);   #afaik, these are just place holders in the vg output as they seem to carry nonsense values
    my $wantedCount = scalar( @wanted );
    my $count = 0;
    my %includeMisc;


    my @variant_seqs = split /,/, $record{alt};
    map {$_ = uc $_} @variant_seqs;    
    #$featureID = $featureIDStart;

      foreach( @infos ){
	  my @temp = split( /=/, $_ );
        if( grep { $_ eq $temp[0] } @wanted ){
          # are there multiple variances?
          my @diffCases = split( /,/, $temp[1] );
          foreach( @diffCases ){
            # see if it has decimal and worthy to be included (i.e !A.XY WHERE XY are not both zero and A is natural num)          
            my @digits =  split ( /\./, $_ );
            my $value;
            if( scalar( @digits ) > 1 ){
              if( $digits[1] > 0 ){                
                $value = $_;   # decimal is worthy, include
                $value =~ s/0{1,}$//; # remove trailing zeroes
              }else{
                $value = $digits[0]; # no decimal, just the whole num / left part of the num in decimal
              }
            }else{
              $value = $_;
            }
            push( @{ $includeMisc{ $temp[0] } },  $value );
#	    print "$temp[0] : $includeMisc{ $temp[0] }[0]\n";
            $count++;
          }
	  if( $count >= $wantedCount ){
	      last;
	  }
	}
      }

      # EG: add sample data
      foreach my $sname  (keys %sampleData) {
	  push( @{ $includeMisc{$sname} }, $sampleData{$sname});
      }

#EG: commenting out the whole ID output construction and just using the id directly from the vcf 'ID' column

# =info    
#     I opted to add "_I" or "_S" to indicate INDEL or SNP, you might want to modify these parts
#     accordingly.
# =cut    
     if( length($reference_seq) > 1 or grep { length $_ > 1 } @variant_seqs ){
       $type = "indel";  # It seems to me, Chado and GBrowse is not okay if this is in CAPS!
# 	      $end = ($start + length($reference_seq)) - 1;
#       $countINDEL += 1;       
# #     $featureID .= ( $separator . "I" . $datasetTag . $countINDEL );
#       $featureID = constructFeatureID( $simpleFeatureName, $separator, "I", $datasetTag, $countINDEL, $featureIDStart );
      }else{
        $type = "SNP";
#        $countSNP += 1;
#        #      $featureID .= ( $separator . "S" . $datasetTag . $countSNP );
#        $featureID = constructFeatureID( $simpleFeatureName, $separator, "S", $datasetTag, $countSNP, $featureIDStart );

      }

      $featureID = $feature_id;
      
      $note = length($reference_seq) . ">"; 
      $note .= join('%2C', map {length($_)} @variant_seqs);  # %2C is url encoding for a comma  
      foreach my $key( sort keys %includeMisc ){
	  $note .= " $key=" . shift(@{ $includeMisc{$key} });
     }
    print $OUThandle "$seqid\t$sourceName\t$type\t$start\t$end\t$score\t$strand\t$phase\tID=$featureID;Name=$featureID;Note=$note\n";
  }
  handle_message( 'NOTICE', 'Finished', "Written to $gff_file ." );
}




sub constructFeatureID {
=doc
  Constructs the appropriate feature ID for the feature concerned.
  
  Arguments:
    0 - int. Use simple names or not?
	1 - string. Separator, if applicable.
	2 - string. "I" or "S" or "" ( indel, SNP, none )
	3 - string. Data set tag
	4 - int. The ordinal number of this feature
    5 - string. Source of the feature ( VCF 2nd column, 1-index)
  Returns:
    String. The constructed feature ID.
=cut
  if ( $_[0] ) {
    return $_[3] . $_[1] . $_[2] .  $_[4];
  }else{
    return $_[5] . $_[1] . $_[2] . $_[3] . $_[4];
  }
} # sub

sub handle_message {
  my $message;
  my ($level, $code, $sentMsg) = @_;
  $sentMsg ||= "No message.";
  chomp $sentMsg;
  $message .= ( localtime() . ' ' . $sentMsg . "\n" );
  if ($level eq 'FATAL') {
    die join ' : ', ($level, $code, $message);
  }else {
    print STDERR join ' : ', ($level, $code, $message);
  }
}
