#!/bin/bash

# Reads a vg-produced vcf file and extracts the length of the alternative allele for the non-zero genotypes matching the selected sequence/prefix,
# then prints out the *difference* vs the reference allele as the "length" of the SV.  The outpu is printed in the form of a set of of fasta sequences -
# one for the referenc allele and one for every alt allese associated with a given bubble (vcf entry)
# Only solid single-path genotypes are considered, i.e. we exclude variants where multiple paths from the same sample are traced through the bubble, for example   3/1:pAurM7.49=1,pAurM5.6=3.


if [ "$#" -ne 2 ]; then
    echo "Usage: $0 [vcf_File] [sequence_name]" 
    exit
fi

if [[ $1 == *.gz ]]; then
    vcfFile=$(basename $filename .gz)
    zcat $1 > $vcfFile
else
    vcfFile=$1
fi

queryName=$2

echo "looking for column with $queryName"
header=$(grep "#CHROM" $vcfFile)

IFS=$'\t' read -ra cols <<< "$header"

gcolumn=''
i=1
for cname in "${cols[@]}"
do
    if [[ "$cname" == *"$queryName" ]]; then
	gcolumn=$i
	break
    fi
    ((i++))
done

if [ -z "$gcolumn" ]; then
    echo "Error: no column header matches the specified sequence name or prefix!"
    exit
fi


cat $vcfFile | grep -v '#' | awk -v gcol="$gcolumn" '{alleleNum=substr($gcol,0,1); offset=0; a=1; {while (a<=3) {altEnd = index( substr($5,offset),"," )-1;  if(a == alleleNum){ altSeq=(altEnd < 0 ? substr($5,offset) : substr($5,offset,altEnd)); svlen=length(altSeq)-length($4); print">"$1,$2,$3,svlen,"refAlleleSeq:\n"$4; print ">"$1,$2,$3,svlen,$gcol,"altAlleleSeq:\n"altSeq} a++; offset=altEnd+2 }} }' 


	      
