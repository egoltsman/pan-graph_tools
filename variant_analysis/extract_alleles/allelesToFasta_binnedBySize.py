#!/usr/bin/env python

"""
Reads a fasta file of SVs in this format:

>pLimC3.1 52131696 222_333 85 1:pAurM3.1=1 altAlleleSeq:
ACCAAAATGCAGGTGGTGCACCAAACGCTGGTGGTTGTTTCTTGTTGTAAGTGAATGCAAGGAAAAGACCCGGAAATACGTAAGGAAGA
>pLimC3.1 52131696 222_333 85 refAlleleSeq:
AAA

bins allele sequences by *indel* size  (not allele size) and prints as fasta all bins with n or more sequences

For insertions, positive indel sizes are considered and the "Alt" allele is printed.
For deletions, negative equivalent of the indel size is considered and the "Ref" allele is printed
"""

import sys
import math

if len(sys.argv) < 3:
    print('Usage: %s <SV file> <bin_size> <<min_in_bin>>' % (sys.argv[0]))
    sys.exit(0)


SVfile = sys.argv[1]
binSize = int(sys.argv[2])
if len(sys.argv) == 4:
    minInBin = int(sys.argv[3])
else:
    minInBin = 2
    
binned = {}

with open(SVfile, 'r') as f:
    myBin = 0
    printme = 0
    for line in f:
        if line[0] == '>':
            printme = 0
            line = str.rstrip(line)
            fields = line.split(' ')
            svlen = int(fields[3])
            if (fields[-1] == 'altAlleleSeq:' and svlen >= 0) or (fields[-1] == 'refAlleleSeq:' and svlen < 0):
                hdr = '__'.join(fields)
                myBin =  binSize * math.floor(svlen/binSize)
                if myBin in binned:
                    binned[myBin].append([hdr,''])
                else:
                    binned[myBin] = [ [hdr,''] ]
                printme = 1
        elif printme:
            #add sequence line to the last element's second position
            binned[myBin][-1][1] += line  
        
for b, seqsList in binned.items():
    #print(b, len(seqsList))
    #assert(isinstance(seqsList, list))
    #assert(isinstance(seqsList[-1], list))
    #assert(isinstance(seqsList[-1][-1], str)) 
           
    if len(seqsList) < minInBin:
        continue
    outFn = SVfile + '.bin_' + str(b) + '.fasta'
    with open(outFn, "w") as fout:
        for entry in seqsList:
            fout.write(entry[0] + '\n' + entry[1] +'\n')

        
