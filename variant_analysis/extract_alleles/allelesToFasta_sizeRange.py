#!/usr/bin/env python

"""
Reads a fasta file of SVs in this format:

>pLimC3.1 52131696 1:pAurM3.1=1 85 altAlleleSeq:
ACCAAAATGCAGGTGGTGCACCAAACGCTGGTGGTTGTTTCTTGTTGTAAGTGAATGCAAGGAAAAGACCCGGAAATACGTAAGGAAGA
>pLimC3.1 52125339 -626 refAlleleSeq:
AAA

and extracts "allele" sequences corresponding to the given indel size constraint.  
For insertions, positive indel sizes are considered and the "Alt" allele is printed.
For deletions, negative equivalent of the indel size is considered and the "Ref" allele is printed
"""

import sys

if len(sys.argv) != 4:
    print('Usage: %s <SV file> <min> <max>' % (sys.argv[0]))
    sys.exit(0)


SVfile = sys.argv[1]
min = int(sys.argv[2])
max = int(sys.argv[3])


with open(SVfile, 'r') as f:
    printme = 0
    for line in f:
        if line[0] == '>':
            printme = 0
            line = str.rstrip(line)
            fields = line.split(' ')
            svlen = int(fields[-2])
            if (fields[-1] == 'altAlleleSeq:' and svlen >= min and svlen <= max) or (fields[-1] == 'refAlleleSeq:' and svlen >= max * -1 and svlen <= min * -1):
                hdr = '__'.join(fields)
                print(hdr)
                printme = 1   
        else:
            if printme:
                print(line)

                
