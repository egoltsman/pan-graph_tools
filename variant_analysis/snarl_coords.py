#!/usr/bin/env python3
"""
Given a vg-produced (!) vcf file of variants and the corresponding GFA file with paths containing query start coordinate (qs:Z)  and offset (of:Z) tags,
extrapolate the variant coordinates *on the sample-sequences*  (as opposed to coords on the path as produced by vg)
"""

import gfapy
import os, getopt, sys
sys.path.append('/global/homes/e/eugeneg/scripts')
from dataclasses import dataclass
from typing import List  #allows type hinting in lists
import re
import json
from collections import OrderedDict

me=os.path.basename(sys.argv[0])
usageStr='usage: ' + me + ' -g [GFA file] -v [vcf file] -o [output file prefix]  -f [ output format (csv|vcf|bed)]  <<-s [sequence name]>>  '

selectSampleName = None  #get coords only on this sample (names are determined from the vcf header) and ignore other sample-sequences that may be traversing the bubble. Defaults to using all samples

refSampleName = 'Ref'  #since the reference sample doesnt have a name in the vcf (only the path name), we define it here

outFmt = 'csv'

if len(sys.argv) < 6:
    print(usageStr)
    sys.exit(2)
try: 
    opts, extras = getopt.getopt(sys.argv[1:], 'g:v:o:s:f:')
    for opt, arg in opts:
        if opt == '-g':
            inGFAFile = arg
        if opt == '-v':
            inVcfFile = arg
        if opt == '-o':
            outPrefix = arg
        if opt == '-s':
            selectSampleName = arg
        if opt == '-f':
            outFmt = arg

            
except getopt.error as err: 
    print (str(err))
    sys.exit(2)


@dataclass
class pathInfo:
    """
    Stores path name , captured segments, and precise start of the alignment underlying the path, both on
    the query sequene (qStart) and on the 1st segment (offset)
    """
    name: str
    segments: List[str]
    segLens: List[int]
    qs: int = -1
    of: int = -1

    def get_seq_name(self):
        # Assumes the following naming convention for the path name: ['p'][seqName].[numericCounter]
        if not self.name:
            return None
        tmp = self.name.lstrip('p')
        return tmp.rsplit('.',1)[0]

def parse_path_info(gfa):
    """
    extract path names, captured segment info, and precise start of the alignment underlying the path, both on
    the query sequene (qStart) and on the 1st segment (offset). Relies on qs:Z and of:Z tags in the path lines
    of the gfa object.
    NOTE: we could as well instead get all the info directly from the gfa object, so this is just an abstraction layer
    """
    
    pathInfoD = {}
    for p in gfa.paths:
        assert (p.qs and p.of), 'path is missing qs:Z and/or of:Z tags in the GFA file'
        #go from orientedLine type to  list of name+ori
        #oriNames = []
        segNames = []
        segLens = []
        for s in p.captured_segments:
            #oriNames.append(str(s.name+s.orient))
            segNames.append(s.name)
            segLens.append(int(s.LN))
        assert len(segNames) == len(segLens)
        
        myPathInfo = pathInfo(p.name, [], [])
        myPathInfo.segments = segNames
        myPathInfo.segLens = segLens
        myPathInfo.qs = int(p.qs)
        myPathInfo.of = int(p.of)

        pathInfoD[p.name] = myPathInfo

    return pathInfoD


                 


def walk_len_to_bubble(gfa, pathInfo, bubbleTipsStr):
    """
    Add up length of all segments up to, and including, the bubble tip (whichever tip occurs first).  If the pair of tips occurs more than once on the path, return 'None'
    """
    bubbleTips = parse_tips(bubbleTipsStr)
    
    #first check if the bubble tip pair is only found once in the path
    if pathInfo.segments.count(bubbleTips[0]) > 1 or pathInfo.segments.count(bubbleTips[1]) > 1:
        print('WARNING: [%s]: path %s contains one or both  bubble tips multiple times. Not sure which one is the bubble to consider. Skipping for now...' % (bubbleTipsStr,pathInfo.name))
        return None
    
    walkLen = 0
    i=0
    for s in pathInfo.segments:
        walkLen +=  pathInfo.segLens[i]
        if s in bubbleTips:
            break
        i += 1
    if i == len(pathInfo.segments):
        raise Exception('bubble tip segment %s was not found in path %s ! Check that vcf matches the GFA file!' % (endS, pathInfo.name))
    return walkLen


def parse_tips(vgTipsString):
    """
    assumes the following format of the tips string:  [>|<][tipL][>|<][tipR], e.g. >s7494>s7496
    """
    assert(vgTipsString[0] == '>' or vgTipsString[0] == '<')
    
    tips = re.split('>|<', vgTipsString)[1:]
    return tips



def parse_genotypes(refPathName, genotypesD):
    """
    Extract path names and the corresponding allele values from the genotypes fields of the vcf file.
    Assumes genotype format from vg-decstruct, e.g.   0:pAurP1.5=0  1:pLimC1.1=1  
    which means traversal path pAurP1.5 matches the reference allele, while path pLimC1.1 matches the 1st alternative "allele"

    There could be "heterozygous alleles" when two (or more) paths from the same sample traverse the same bubble:

    1/0:pAurP5.67=0,pAurP9.7=1

    meaning a 1/0 genotype on sample pAur invoves a reference allele on path pAurP5.67 and the first alternative allele on path pAurP9.7  (note: in these cases it's always 1/0, never 0/1). 

    Returns a dict of dicts, e.g.

    {sampleA : {path1 : allele, path2 : allele}}
    """
    data = OrderedDict()
    data[refSampleName] = {refPathName : 0}  # initialize; ref path gets a reference allele value (0) by default)
    
    for s, gt in genotypesD.items():
        assert(':' in gt),'Invalid genotype string'
        data[s] = {}
        if gt == '.:.':  # null/uncalled
            continue
        (gtSignature, allelesStr) = gt.split(':')
        alleles = allelesStr.split(',')
        for a in alleles:
            (p, av) = a.split('=')
            data[s][p] = av

    return data




def var_pos_on_samples(snarlTipsStr, GTInfo, pathInfoD):
    """
    Based on genotypes on each sample-path, extrapolate the variant position(s) on the sample sequence which underlies it.
    Returns a 2d dictionary of lists  (or lists-of-lists if multipl paths from the same sequence traverse the bubble)
    """

    onSeqData = OrderedDict()  #maintain sample order 
    
    for smplName, sampleGTInfo in GTInfo.items():

        onSeqData[smplName] = {}

        if not len(sampleGTInfo): #we could end up with no genotype info for a given sample -that's ok, but we still store the empty slot
            continue

        for p, alleleVal in sampleGTInfo.items():
            myPathInfo = pathInfoD[p]
            myContigName = myPathInfo.get_seq_name()
            walkLen = walk_len_to_bubble(gfa, myPathInfo, snarlTipsStr)
            if not walkLen:
                if smplName == refSampleName:
                    #for the ref seq, we can determine the position straight from posOnRefPath we got from the input vcf
                    pos = myPathInfo.qs + posOnRefPath  - myPathInfo.of
                    print('WARNING: [%s] : bubble position for reference seq determined from vcf input (column 2)' % ( snarlTipsStr))
                    
                else:
                    print('WARNING: [%s] : could not determine unique bubble position for path %s' % ( snarlTipsStr, myPathInfo.name))
                    onSeqData[smplName][myContigName] = []
                    continue
            else:
                pos = myPathInfo.qs + walkLen - myPathInfo.of

            #        print('DBG: seq name parsed out: ', myContigName)
            if myContigName in onSeqData[smplName].keys():
                print('WARNING: [%s] : multiple paths from the same sequence are associated with the bubble! ' % (snarlTipsStr))
                onSeqData[smplName][myContigName].append([str(pos),str(alleleVal)])
            else:
                onSeqData[smplName][myContigName] = [[str(pos),str(alleleVal)]]


    if not len(onSeqData):
        raise  Exception('No sequence data was obtained for any sample!' % ())
    
    return onSeqData



if __name__ == "__main__":

    gfa=gfapy.Gfa.from_file(inGFAFile)

    pathInfoD = parse_path_info(gfa)

    sampleNames = [] 
    #read the vcf header to get the query sample names - we need them before defining the output files
    with open(inVcfFile) as vcf:
        rows = ( line.rstrip().split('\t') for line in vcf )
        for row in rows:
            #vcf header:  #CHROM  POS     ID      REF     ALT     QUAL    FILTER  INFO    FORMAT  pAurP   pLimC   pLimO
            if row[0].startswith('#CHROM'):
                sampleNames = row[9:]
                if selectSampleName:
                    assert(selectSampleName in sampleNames), 'None of the sample names appearing in the vcf header match the user-provided sample seletion!'
                break

    # add the ref
    sampleNames.append(refSampleName)

    
    if outFmt == 'csv' or outFmt == 'vcf':
        outFN = outPrefix + '.' + outFmt
        of = open(outFN, 'w')
    elif outFmt == 'bed':
        # open a separate bed file for every sample (or selected sample); it will be populated with sample-specific bubble info
        bedHandles = {}
        for sample in sampleNames:
            if selectSampleName and sample != selectSampleName:
                continue
            bedFN = outPrefix + '.' + sample + '.' + outFmt
            bedHandles[sample] = open(bedFN, 'w') 
    else:
        print('Input Error: unsupported output format specified!')
        exit(1)


    if outFmt == 'csv':
        col2_header = selectSampleName if selectSampleName else 'SEQ-TRAVERSAL_DATA'
        of.write('varId\t%s\n' % (col2_header))

            

    with open(inVcfFile) as vcf:
        rows = ( line.rstrip().split('\t') for line in vcf )
        for row in rows:
            #vcf header:  #CHROM  POS     ID      REF     ALT     QUAL    FILTER  INFO    FORMAT  pAurP   pLimC   pLimO
            if row[0].startswith('#'):
                if outFmt == 'vcf':
                    of.write('\t'.join(row) + '\n')
                continue
            refPathId = row[0]  
            posOnRefPath = int(row[1])  #The second column in a vg-produced (!) variant vcf is the position *on the path* 
            snarlTipsStr = row[2]
            refSeq,altSeqs,qual,filter,info,fmt =  [row[i] for i in range(3,9)]

            if outFmt == 'bed':
                # bed output needs allele lengths to determine feature (i.e. bubble) end positions
                alleleLens = [len(refSeq)]
                for seq in altSeqs.split(','):
                    alleleLens.append(len(seq))
                              
            genotypesD = {sampleNames[i] : row[(9+i)] for i in range(len(sampleNames)-1)}  #minus 1 because sampleNames includes the reference sample, but there's not genotype entry for it
            assert len(genotypesD.keys())
            
            #extract non-ref paths and allele values
            myGTInfo = parse_genotypes(refPathId, genotypesD)
            #print('DBG: ', snarlTipsStr, myGTInfo)
            #print('DBG: pathInfo:', pathInfoD.keys())
            for s, gt in myGTInfo.items():
                #print(gt)
                for p in gt.keys():
                    assert p in pathInfoD.keys(), 'Path names extracted from the vcf and gfa files do not match!' 
                
            bubbleOnSeqData = var_pos_on_samples(snarlTipsStr, myGTInfo, pathInfoD)
            
            #VALIDATION
            #for the reference path, we don't really need to use the walk-to-bubble approach since we already have the coordinate on the path. Here we use it
            #for validation.
            #NOTE: If a select non-ref sample name was passed in, this step is skipped
            if not selectSampleName:    
                myPInfo = pathInfoD[refPathId]
                refPosValidate = myPInfo.qs + posOnRefPath - myPInfo.of
                refSeqName = myPInfo.get_seq_name()
                if refPosValidate != int(bubbleOnSeqData[refSampleName][refSeqName][0][0]):
                    print('WARNING: bubble %s: refPosValidate: %d  differs from walk-derived position %s' % (snarlTipsStr, refPosValidate, bubbleOnSeqData[refSampleName][refSeqName][0][0])) 


                    
            """
            Write as [bubbleName], [json strings of the seqData dicts for each sample]. e.g
            >s1>s3  {"LimC1": [["111190","0"]]}     {"LimO1": [["105430","1"]],"LimO5": [["105430","0"]]}

            
            # If vcf output is specified,  include other vcf fields up untill the genotypes. The original genotype fields will be replaced with the processed seqData, e.g.,
            LimC1   111190  >s1>s3 ATGACTCTCTCACTATTTTTCCCTTAGTTAGACTCAAATTGGCAAGGGGAT     AGACTCAAATTGGCAAGGGGA   60      .       AC=1;AF=0.333333;AN=3;LV=0;NS=3 GT:PI   {"LimC1": [["111190","0"]]}     {"LimO1": [["105430","1"]],"LimO5": [["105430","0"]]
            """

            #bubbleOnSeqData is a  *dict of dicts of lists*  where the values have the format [ [positionOnSeq, alleleValue], [position,alleleValue], [...] ]


            if outFmt == 'vcf':
                refSeqName = pathInfoD[refPathId].get_seq_name()
                #there could be cases where the position on the referece sequence couldn't be
                if not len(bubbleOnSeqData[refSampleName][refSeqName]):
                    continue
                of.write(refSeqName)
                of.write('\t' + bubbleOnSeqData[refSampleName][refSeqName][0][0])
                of.write('\t' + snarlTipsStr)
                of.write('\t' + '\t'.join([refSeq,altSeqs,qual,filter,info,fmt]))
                for sample, seqData in bubbleOnSeqData.items():
                    if selectSampleName and sample != selectSampleName:
                        continue
                    if sample == refSampleName:
                        #don't output the coords for the ref seq
                        continue
                    jsonStr = json.dumps(seqData, indent=0).replace("\n","")
                    of.write('\t' + jsonStr)
                of.write('\n')
                
            elif outFmt == 'csv':
                of.write(snarlTipsStr) 
                jsonStr = json.dumps(bubbleOnSeqData, indent=0).replace("\n","")
                of.write('\t' + jsonStr + '\n')

            elif outFmt == 'bed':
                for sample in sampleNames:
                    if selectSampleName and sample != selectSampleName:
                        continue
                    for seqName, seqRecords in bubbleOnSeqData[sample].items():  # a bubble can be associated with  multiple sequences from same sample
                        for record in seqRecords:  #.... and/or multiple positions on the same sequence
                            bublStart,allele = record
                            alleleLen = alleleLens[int(allele)]
                            bublEnd = int(bublStart) + int(alleleLen)
                            bedLine = '\t'.join([seqName,str(bublStart),str(bublEnd),snarlTipsStr]) 
                            bedHandles[sample].write(bedLine + '\n')


                
                    

