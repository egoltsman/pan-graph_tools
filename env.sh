#!/bin/bash

#sets some boilerplate variables used throughout the scripts. Also, loads Python3 and the conda environment used by many of the scripts



if [[ (-z "$PYTHONPATH") || (! `command -v python3`) || (! `command -v conda`) ]]; then
    echo "Anaconda Python 3 is required in this environment. Will attempt to load with a module";
    available="$(module avail python 2>&1)"
    if [[ -z $available ]]; then
	echo "No python modules found!"; exit;
    else
	module load python  #should default to anaconda python3
	command -v python3 > /dev/null || (echo "FATAL: Python3 was not loaded"; exit 1)
	command -v conda > /dev/null || (echo "FATAL: Current python version doesn't support conda environments"; exit 1)
	echo "Python 3 loaded successfully"
    fi
fi
   
			 

source activate /global/homes/e/eugeneg/.conda/envs/gfa+ || (echo "Warning: conda environment 'gfa+' not found; some GFA analysis scripts may not work without this environment activated.")


if [ -z "$UTILS_PATH" ]; then
    UTILS_PATH=/global/homes/e/eugeneg/utils
fi

if [ -z "$SCRIPTS_PATH" ]; then
    SCRIPTS_PATH=/global/homes/e/eugeneg/scripts/pangenome_graph
fi

export PATH=/global/homes/e/eugeneg/bin/vg:${PATH}
