#! /usr/bin/env python3
'''
Calculate minimizers and store them in a separate hash table for every sequence in the input fasta.  This allows
for parallelization (TODO) at the expense of redundant minimizer storage, i.e. more memory
'''

import os, getopt, sys
from pympler.asizeof import asizeof

sys.path.append('/global/homes/e/eugeneg/scripts')
from timer import Timer

script=os.path.basename(sys.argv[0])
usageStr='Usage: ' + script + ' -f <sequence_file>  << -w <window_size> >>  << -m <minimizer_size> >> << -t <num_of_threads> >>   ' 

if len(sys.argv) < 2:
    print(usageStr)
    sys.exit(2)


Win = 31
M = 7
threads=12
t = Timer()
    
opts, extras = getopt.getopt(sys.argv[1:], 'f:w:m:t:')
for opt, arg in opts:
    if opt == '-f':
        seqFile = arg
    if opt == '-w':
        Win = int(arg)
    if opt == '-m':
        M = int(arg)
    if opt == '-t':
        threads = int(arg)




def get_minimizers(M, Win, seq, minimizerCountsD):
    '''
    determine minimizers in the sequence and accumulate them and their positions in the passed-in dictionary
    '''
    t.start()
    if any(c.islower() for c in seq):
        seq = seq.upper()
    rev=seq[::-1]

    rev=rev.replace("A","X")
    rev=rev.replace("T","A")
    rev=rev.replace("X","T")
    rev=rev.replace("C","X")
    rev=rev.replace("G","C")
    rev=rev.replace("X","G")

    L=len(seq)
    minPrev=''
    for i in range(0, L-Win+1):

        sub_f=seq[i:i+Win]
        sub_r=rev[L-Win-i:L-i]

        min="ZZZZZZZZZZZZZZZZZZZZZZZZZZZZ"
        for j in range(0, Win-M+1):
                sub2=sub_f[j:j+M]
                if sub2 < min:
                        min=sub2
                sub2=sub_r[j:j+M]
                if sub2 < min:
                        min=sub2
        if min != minPrev:
                #print(sub_f,min)
                #print(min)
                if min in minimizerCountsD:
                    #print('a')
                        #minimizerCountsD[min] += 1
                    minimizerCountsD[min].append(i)
                    #print('dict size: ', asizeof(minimizerCountsD), ' bytes') 
                else:
                    #print('i')
                        #minimizerCountsD[min] = 1
                    minimizerCountsD[min] = [ i ]
                    #print('dict size: ', asizeof(minimizerCountsD), ' bytes') 
                minPrev = min


    print('dict len: ', len(minimizerCountsD),' dict size: ', asizeof(minimizerCountsD), ' bytes')
    t.stop()

    
#TEST
#testD={}
#get_minimizers(7, 31, "ATGCGATATCGTAGGCGTCGATGGAGAGCTAGATCGATCGATCTAAATCCCGATCGATTCCGAGCGCGATCAAAGCGCGATAGGCTAGCTAAAGCTAGCA", testD)
#assert( len(testD) == 7 )


if __name__ == "__main__":

    minimizerHashes = []  # a list to store minimizer hash tables for every sequence

    with open(seqFile) as f:
        seq=''
        for line in f:
            if line.startswith('>'):
                if seq:
                    minimizerHashes.append({})
                    get_minimizers(M, Win, seq, minimizerHashes[-1])
                    print('hashOfDict len: ', len(minimizerHashes),' hashOfDict size: ', asizeof(minimizerHashes), ' bytes')
                    seq=''
            else:
                seq += line.rstrip()
        if seq:
            minimizerHashes.append({})
            get_minimizers(M, Win, seq, minimizerHashes[-1])
            print('hashOfDict len: ', len(minimizerHashes),' hashOfDict size: ', asizeof(minimizerHashes), ' bytes')
        
        for i in range(len(minimizerHashes)):
            nMins = 0
            nLocs = 0
            for v in minimizerHashes[i].values():
                nMins += 1
                nLocs += len(v)
            print ('seq: ', i, 'nMins: ', nMins, ' nLocs: ', nLocs)

