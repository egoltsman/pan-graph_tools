#!/usr/bin/env python3
#Read gaf alignment file produced by minigraph and load path of minimum size into given GFA file

import os, getopt, sys
import gfapy

sys.path.append('/global/homes/e/eugeneg/scripts')
import my_graph_ops
from my_graph_ops import *

inGfaFile = None
outGfaFile = None

script=os.path.basename(sys.argv[0])
usageStr='usage: ' + script + ' -g <GFA[1|2] file> -o <output GFA file> -v <version (1|2) ' 
if len(sys.argv) < 6:
    print(usageStr)
    sys.exit(2)
try: 
    opts, extras = getopt.getopt(sys.argv[1:], 'g:o:v:')
    for opt, arg in opts:
        if opt == '-g':
            inGfaFile = arg
        if opt == '-o':
            outGfaFile = arg
        if opt == '-v':
            outGfaVersion = arg
            
            
except getopt.error as err: 
    print (str(err))
    sys.exit(2)


if outGfaVersion not in ['1','2']:
    print ('Invalid output GFA version')
    print(usageStr)
    sys.exit(2)


with open(inGfaFile) as f:
    ln1 = f.readline()
    if ln1.startswith('H\tVN:Z:1'):
        gfa=gfapy.Gfa(version='gfa1')
        gfa=gfapy.Gfa.from_file(inGfaFile)

    elif ln1.startswith('H\tVN:Z:2'):
        gfa=gfapy.Gfa(version='gfa2')
        gfa=gfapy.Gfa.from_file(inGfaFile)
        
    else:
        #assume its rGFA if header is missing
        gfa=gfapy.Gfa(version='gfa1',dialect='rgfa')
        gfa=gfapy.Gfa.from_file(inGfaFile, dialect="rgfa")
        #from here on, treat the gfa object as regular GFA1 file
        gfa.dialect='gfa'
        gfa.add_line("H\tVN:Z:1.0")
        
print ('input GFA version %s detected and loaded successfully. Will convert to GFA %s' % (gfa.version, outGfaVersion)) 
                                  

of = open(outGfaFile, "x")



if gfa.version == 'gfa1' and outGfaVersion == '2':
    #gfa_s = gfa.to_gfa2_s()
    gfa_converted = gfa.to_gfa2()        
elif gfa.version == 'gfa2' and outGfaVersion == '1':
    #gfa_s = gfa.to_gfa1_s()
    gfa_converted = gfa.to_gfa1()
else:
    print ('Error: could not perform the conversion from ', gfa.version, 'to ', outGfaVersion)
    sys.exit(2)


# The gfapy convertion module doesn't convert path tags  (Bug? Contact author!), so we add them manually. Note the hard-coded tag name!
for p in gfa.paths:
    name = p.name
    gfa_converted.line(name).ac = p.ac 


gfa_converted.to_file(outGfaFile)
#print(gfa_s, file = of)

print('Done.')
    

