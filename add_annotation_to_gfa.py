#!/usr/bin/env python3

# Adds annotations from the gff file as Fragments to graph Segments that appear in the paths traced by the specified sample.
# Outputs GFA2


import os, getopt, sys
from os import path
import re
import time
import logging as log

try:
    import gfapy
except ModuleNotFoundError:
    sys.stderr.write("ERROR: Module 'gfapy' is not installed! Load the 'gfa+' conda environment or install gfapy on your system!")
    exit(1) 
try:
    import gffutils
except ModuleNotFoundError:
    sys.stderr.write("ERROR: Module 'gffutils' is not installed! Load the 'gfa+' conda environment or install gfapy on your system!")
    exit(1) 


sys.path.append('/global/homes/e/eugeneg/scripts')
from timer import Timer

featureTypes = ['gene']  #default feature

# mode P: add annotations to all segments of sample-paths matching mySample
# mode I: add annotations only to sample-induced segments of the path
mode = 'P'
verbose=0

if len(sys.argv) < 8:
    script=os.path.basename(__file__)
    print ("usage: %s -a <gff file> -g <GFA1|2 file> -s <sequence/sample name> -o <output GFA2 file> [-f <feature type(s)>] [-I(nduced_nodes_only)]" %(script))
    sys.exit(2)
try: 
    opts, extras = getopt.getopt(sys.argv[1:], 'a:f:g:s:o:v:IV')
    for opt, arg in opts:
        if opt == '-a':
            inAnnotFile = arg
        if opt == '-g':
            inGfaFile = arg
        if opt == '-s':
            mySampleName = arg
        if opt == '-o':
            outGfaFile = arg
        if opt == '-f':
            featureTypes = arg.split(",")
        if opt == '-I':
            mode = 'I'
        if opt == '-V':
            verbose = 1
            
except getopt.error as err: 
    print (str(err)) 
    sys.exit(2)

if verbose:
    log.basicConfig(format="%(levelname)s: %(message)s", stream=sys.stdout, level=log.INFO)  #change this to DEBUG for full range of messages
    log.info("Verbose output.")
else:
    log.basicConfig(format="%(levelname)s: %(message)s",stream=sys.stdout)
    

start_overall = time.time()
t = Timer()



def add_features(mode, gfa2, segments):
    feature_placer = get_feature_placer(mode)
    return feature_placer(gfa2, segments)

def get_feature_placer(mode):
    if mode == 'P':
        return _add_features_to_path
    elif mode == 'I':
        return _add_features_to_induced_segments
    else:
        raise ValueError(mode)

def _add_features_to_induced_segments(gfa2, segments):
    """
    Adds features as Fragment lines (GFA2 only) to segments that were induced by mySample.
    Input segments ( assumed to come from sample-paths ) can have different Stable Origins and, therefore, different coordinate offsets
    We isolate only those segments that match our sample and then sort them if necessary
    """
    
    my_segments = []
    for s in segments:
        if s.SN == mySampleName:
            my_segments.append(s)
    
    if len(my_segments) == 0:
        #raise Exception('No segments found that were induced by provided sample name')
        log.warning('No segments found that were induced by provided sample name')
        return 0
    
    log.info('Found %d segmens induced by %s', len(my_segments), mySampleName)

    ## ASSERTION TEST
    #Path elements induced byt *this* sample  should be in linear increasing order (TODO: check for other possibilities)
    #prev_SO = -1
    #for my_s in my_segments:
    #    print('DBG: ',my_s.sid, my_s.SN, my_s.SO, my_s.SR)
    #    assert (my_s.SO > prev_SO), "Paths segments\' SO positions must be increasing"     
    #    prev_SO =  my_s.SO;
    #print('\n')
    
    #return strings for 1-item results, instead of list
    gffutils.constants.always_return_list = False
    
    cnt = 0
    #!!!!! TODO: OPTIMIZE THIS by sorting features by pos
    for featType in featureTypes:
        print('adding features of type ',featType)
        featCnt = db.count_features_of_type(featuretype=featType)
        if featCnt:
            log.info('%d  features of type %s found in the database',featCnt, featType)
        else:
            log.error('No features of type %s found in the database', featType )
            sys.exit(2)

        for feat in db.features_of_type(featType):
            if feat.end < int(my_segments[0].SO) or feat.start > int(my_segments[-1].SO) + int(my_segments[-1].LN) - 1:
                log.debug('feature lies outside of this path; skipping..')
                continue
            log.debug('feature: %s%s %d-%d: mapping to path segments....', feat['ID'], feat.strand, feat.start, feat.end)
            for my_s in my_segments:
                segStart = int(my_s.SO)
                if segStart + int(my_s.LN)  - 1 < feat.start:
                    continue
                if segStart >= feat.end:
                    break
                log.debug(my_s.sid, my_s.SO, my_s.SR, my_s.LN)

                fragLine = create_F_line(feat, my_s, segStart)
                gfa2.add_line(fragLine)
                
                cnt +=1
    return cnt


def _add_features_to_path(gfa2, segments):
    """

    Adds gff features as Fragment lines (GFA2 only) to segments.  The segments only carry their original "source offsets"
    and not the mySample-relative coords.  Because of this, in addition to the position of the Fragment on the Segment we 
    record (as tags) the  original feature coordinates from the gff.
    """
    #return strings for 1-item results, instead of list
    gffutils.constants.always_return_list = False

    mySegmentCoords = map_coords(segments, mySampleName)
    if not len(mySegmentCoords):
        return 0

    
    # find matching segment(s) for each feature
    cnt = 0
    for featType in featureTypes:
        print('adding features of type ',featType)
        featCnt = db.count_features_of_type(featuretype=featType)
        if not featCnt:
            log.error('No features of type %s found in the database', featType)
            sys.exit(2)
        else:
            log.info('%d  features of type %s found in the database',featCnt, featType) 
        
        for feat in db.features_of_type(featType):
            if feat.end < mySegmentCoords[segments[0].name] or feat.start > mySegmentCoords[segments[-1].name] + int(segments[-1].LN) - 1:
                log.debug('\tfeature %s lies outside of this path; skipping..', feat['ID']) 
                continue
            
            log.debug('feature: %s%s %d-%d: mapping to path segments....', feat['ID'], feat.strand, feat.start, feat.end)
            for my_s in segments:
                segStart = mySegmentCoords[my_s.name]
                if segStart + int(my_s.LN)  - 1 < feat.start:
                    continue
                elif segStart >= feat.end:
                    break
                else:
                    #print('DBG: ',my_s.sid, my_s.SO, my_s.SR, my_s.LN, 'SEGMENT_POS_ON_SAMPLE: ',segStart)
                    fragLine = create_F_line(feat, my_s, segStart)
                    gfa2.add_line(fragLine)
                    
                    cnt += 1
    return cnt
                


def create_F_line(feat, my_s, segStart):
    """
    Builds the fragment 'F' line in GFA2 format
    """
    # use zero-based coords throughout;
    segLen = int(my_s.LN)
    fragStartOnSeg =  feat.start - segStart if segStart < feat.start else 0
    fragEndOnSeg = feat.end - segStart  if segStart + segLen - 1 > feat.end else segLen - 1
    startOnFrag = 0
    endOnFrag = fragEndOnSeg - fragStartOnSeg

    # feat.attributes.items() returns a list of tupples; we need to add them one by one
    attributeNames = [ str(i[0]) for i in feat.attributes.items() ]

    
    if 'Name' in attributeNames:
        featName=feat['Name']   # This will be the defacto name of the fragment. Can use feat['ID'] instead
    elif 'ID' in attributeNames:
        featName=feat['ID']
    else:
        log.error('Gff file entries must coinain either \'Name\' or \'ID\' attribute')
        sys.exit(1)

        
    featNameAndOri = featName+feat.strand
    
    lineL = ['F', my_s.sid, featNameAndOri, fragStartOnSeg, fragEndOnSeg, startOnFrag, endOnFrag, '*']
    

    globalPosTag = 'po:Z:' + str(feat.start) + '-' + str(feat.end)
    lineL.append(globalPosTag)

    seqOriTag = 'sn:Z:' + mySampleName  #this is the sequence that the feature originated from, NOT the SN tag of the segment!
    lineL.append(seqOriTag)
    
    attribTagsToAdd = {'ID' : 'id:Z:',
                       'Note' : 'nt:Z:',
#                       'Name' : 'na:Z:',
                       'class' : 'cl:Z:',
                       'synthenic_cluster_genes' : 'sy:Z:' }
                        # add other custom attributes as tags here if needed..
    for attrName, tagName in attribTagsToAdd.items():
        if attrName in attributeNames:
            if isinstance(feat.attributes[attrName], list):
                attr = ','.join(feat.attributes[attrName])
            else:
                attr = feat.attributes[attrName]
            attrTag=tagName + str(attr)
            lineL.append(attrTag)
                
    fragLine = '\t'.join(map(str,lineL))
    log.debug('%s',fragLine)
    
    return fragLine




def map_coords(path_segments, mySampleName):
    """
    Given a list of segemnts, maps their positions to a single linear coordinate system relative to mySample.  
    
    First, identifies 1st segment with SN matching that of mySampleName as our Point of Reference and assign the rest of the segments' coordinates relative to this segment
    Then coordinates are assigned to every segment of the path.

    IMPORTANT: It's possible for a sample-path to have no segments induced by the sample  (SN != mySampleName).  In this case the function returns an empty coords map
    """

    coordsMap = {}
        
    #validate pth to ensure it includes at least one segment induced by our sample. 
    allgood = 0
    for s in path_segments:
        if s.SN == mySampleName:
            allgood = 1
            break
    if not allgood:
        log.warn('No segments induced by %s found in path', mySampleName)
        return coordsMap
        

    offset = None
    bpWalked = 0  #nr of bp walked

    #determine the offset to be applied to the positions of the path based on the SO tag of the 1st segment induced by our sample
    for seg in path_segments:
        if seg.SN == mySampleName:
            offset = int(seg.SO) - bpWalked
            log.info('anchor segment: %s: bpWalked: %d, offset: %d', seg.name, bpWalked, offset )
            break
        else:
            bpWalked += int(seg.LN)
    if offset is None:
        log.error('Could not determine path offset coordinate for sample %s', mySampleName)
        sys.exit()

    #Reset and populate the coordinate map for the sample
    bpWalked = 0
    for seg in path_segments:
        pos = offset + bpWalked
        log.info('seg: %s, SN: %s, bpWalked: %d, offset: %d, pos: %d', seg.name, seg.SN, bpWalked, offset, pos )
        
        """
         In a perfectly construced graph we would expect the SO tag value in sample-induced segments to always match the position we've determined from the walk.
         In reality, segments that show up as non-divergent may actually differ in sequence/size between aligned samples, which will lead to difference between
         the walk length and the original SO tag (added during graph construction)
         THIS IS MY UNTESTED THEORY!
        """
        if seg.SN == mySampleName and int(seg.SO) != pos:
            log.warning("(%s) path-mapped coordinate %d != SO tag value %s  (diff = %d); Resetting coordinate and offset to match SO.. ", seg.name, pos, seg.SO, (pos-int(seg.SO)) )
            
            offset = int(seg.SO) - bpWalked
            pos = int(seg.SO)
            log.info('NEW - offset: %d, pos: %d', offset, pos) 
            
        coordsMap[seg.name] = pos
        bpWalked += int(seg.LN)

    return coordsMap






### MAIN

print('Creating a gffutils database ...')
dbFName = inAnnotFile + '.db'
if path.exists(dbFName):
    print('Existing db file found: ',dbFName,' - will read from this db')
    db = gffutils.FeatureDB(dbFName)
else:
    t.start()
    db = gffutils.create_db(inAnnotFile, dbfn=dbFName, force=True, keep_order=True, merge_strategy='merge', sort_attribute_values=True)
    t.stop()


print('Creating a gfapy object ...')
t.start()
gfa=gfapy.Gfa.from_file(inGfaFile)

if gfa.version == 'gfa1':
    print('\tGFA1 format detected from input')
    gfa2 = gfa.to_gfa2()
elif gfa.version == 'gfa2':
    print('\tGFA2 format detected from input')
    gfa2 = gfa
else:
    raise Exception('Invalid header in gfa file; must be v 2.0 or 1.0')
t.stop()

for p in gfa2.paths:
    # extract sample name from path name; expect this format <p>[sample]<.suffix>

    m = re.search('p?([^.]+)(\.\d+$)?', p.name)
    if not m:
        raise Exception('Invalid path name; must follow format <p>[sample]<.suffix>')
    sampleExtr = m.group(1)
    print('Path ',p.name, '...\n') 

    if (sampleExtr == mySampleName):
        print(f'Adding annotation features to saple-path {p.name} of ', len(p.captured_segments), ' segments ...')
        t.start()
        nFeatAdded = add_features(mode, gfa2, p.captured_segments)
        print("Added  %d features to %s" % (nFeatAdded, p.name))
        t.stop()
    else:
        print('\tSkipping this path')
        pass

gfa2.to_file(outGfaFile)

elapsed_overall = time.time() - start_overall
print(f'All done in {elapsed_overall} s')
    

