#!/usr/bin/env python3
#Read gaf alignment file produced by minigraph and load path of minimum size into given GFA file

import os, getopt, sys

try:
    import gfapy
except ModuleNotFoundError:
    sys.stderr.write("ERROR: Module 'gfapy' is not installed! Load the 'gfa+' conda environment or install gfapy on your system!")
    exit(1)

sys.path.append('/global/homes/e/eugeneg/scripts')
import my_graph_ops
from my_graph_ops import *


prefix='p'  # add this string to every path name 
outGfaVersion = 1
validatePaths = False
trimPathsToGraph = False
minPathElem = 3
inAlignFile = None
inGfaFile = None
outGfaFile = None
maxRank = None
sampleName = None   

script=os.path.basename(sys.argv[0])
usageStr='usage: ' + script + ' -a <GAF file> -g <GFA file> -o <output GFA file> -m <min elements in path> <<-p prefix>> <<-N sampleName>> <<-v [1|2]>> << -V validate?>>  << -R maxRank >> <<-T trim?>> ' 

#TODO: add cutoffs for alignments length/quality, etc.
if len(sys.argv) < 9:
    print(usageStr)
    sys.exit(2)
try: 
    opts, extras = getopt.getopt(sys.argv[1:], 'a:g:o:m:p:v:N:R:TV')
    for opt, arg in opts:
        if opt == '-a':
            inAlignFile = arg
        if opt == '-g':
            inGfaFile = arg
        if opt == '-o':
            outGfaFile = arg
        if opt == '-m':
            if int(arg) < 2:
                print ('Mininum number of path elements (-m)  must be 2 or higher!')
                sys.exit(1)
            minPathElem = int(arg)
        if opt == '-p':
            prefix = arg
        if opt == '-v':
            outGfaVersion = int(arg)
        if opt == '-N':
            sampleName = arg 
            sys.stderr.write("WARNING about -N parameter: Mapping-derived paths can potentially cross samples. Including this in every path line (as \'ac:i\' tag) assumes that all mapped paths in the gfa are sample-coherent, but we don't verify this to be true.\n");

        if opt == '-R':
            maxRank = int(arg)
        if opt == '-V':
            validatePaths = True
        if opt == '-T':
            trimPathsToGraph = True
            
            
except getopt.error as err: 
    print (str(err))
    sys.exit(1)


if inAlignFile is None or inGfaFile is None or outGfaFile is None:
    print ('Missing/invalid input!')
    print(usageStr)
    sys.exit(1)

    
with open(inGfaFile) as f:
    ln1 = f.readline()
    if ln1.startswith('H\tVN:Z:1'):
        gfa=gfapy.Gfa(version='gfa1')
        gfa=gfapy.Gfa.from_file(inGfaFile)

    elif ln1.startswith('H\tVN:Z:2'):
        print('Error:  GFA2 format detected - not yet supported!')
        sys.exit() 
    else:
        #assume its rGFA if header is missing
        gfa=gfapy.Gfa(version='gfa1',dialect='rgfa')
        gfa=gfapy.Gfa.from_file(inGfaFile, dialect="rgfa")
        #from here on, treat the gfa object as regular GFA1 file
        gfa.dialect='gfa'
        gfa.add_line("H\tVN:Z:1.0")


#paths_mapped = {}

from collections import defaultdict  #
paths_mapped = defaultdict(list)   #This will allow to create a new list object if the key is not found in the dictionary. Saves us from having to initialize a paths array for every subject



# alignments in gaf file look like this:
#[qseq name] [qseq len] [qStart] [qEnd] [orient] [segments] [subject len] [sStart] [sEnd] [nMatch] [nMapped] [qual] 
#LimC1   36988349        6       36988344        +       >s12677>s127553>s12678 36988349        6       36988344        35283056        36988338        60 
with open(inAlignFile) as alnIn:
     rows = ( line.split('\t') for line in alnIn )
     for row in rows:
         qSeqId = row[0]
         qStart = row[2]
         segments = row[5]
         sOffset = row[7]   #offset refers to the position where the alignment of the sample-sequenc to the 1st segment actrually starts, i.e. sStart in the .gaf file
         
         entry = '_'.join([segments,qStart,sOffset])
         paths_mapped[qSeqId].append(entry)
nTotalPaths = 0
for subj, paths in paths_mapped.items():
    uniqPathCntr = 1
    pathName = prefix + subj
    for item in paths:
        vals = item.split('_')
        p = vals[0]
        qStart=vals[1]
        offset=vals[2]
        for directn in ('<','>'):
            p = p.replace(directn,'$'+directn)            
            elements = p.split('$')
            elements.pop(0)
            
        if len(elements) >= minPathElem:
            
            if trimPathsToGraph:
                trimmedElements = trim_path_to_graph(elements, gfa)
                if not len(trimmedElements):
                    #print('DBG: {}.{}: no elements left in path after trimming'.format(pathName, uniqPathCntr))
                    uniqPathCntr += 1
                    continue
                print('Trimmed path of ', len(elements), ' to ', len(trimmedElements), ' elements.')
                elements = [ e for e in trimmedElements ]
            if validatePaths:
                if not path_is_graph_coherent(elements, gfa, maxRank):
                    print('Skipping path as it failed validation against the graph: \n', *elements,'\n')
                    continue
                        
            pathFinal=[]
            for e in elements:
                if e[0] == '>':
                    e += '+'
                elif e[0] == '<':
                    e += '-'
                else:
                    raise Exception('ERROR: illegal/missing direction identifier in path element')
                    
                e = e[1:] #remove original direction prefix
                pathFinal.append(e)    

            pathFinalStr = ','.join(pathFinal)
            pathLineStr = 'P\t' + pathName + "." + str(uniqPathCntr) + '\t' + pathFinalStr + '\t*'
            if sampleName:
                pathLineStr += '\tac:Z:' + sampleName
            if qStart and offset:
                pathLineStr += '\tqs:Z:' + qStart + '\tof:Z:' + offset
            line = gfapy.Line(pathLineStr)
            assert (type(line) == gfapy.line.group.path.path.Path), "Line object is not a valid path type"
            gfa.add_line(line)
            gfa.validate()
            uniqPathCntr += 1
            nTotalPaths += 1
            
                                  
if outGfaVersion == 2:
    gfa2 = gfa.to_gfa2()        
    gfa2.to_file(outGfaFile)
else:
    gfa.to_file(outGfaFile)

print('\tAdded ', nTotalPaths,' paths to GFA')
    

