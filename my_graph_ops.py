import sys

import gfapy
from gfapy.sequence import rc

import pandas as pd
from dataclasses import dataclass
from typing import List  #allows type hinting in lists
import enum
import json


@dataclass
class Visit:
    # The node ID or nested child snarl of this step (only one should be given)
    node_id: int = None
    snarl_id: str = None

    # Indicates:
    #   if node_id is specified     reverse complement of node
    #   if snarl is specified       traversal of a child snarl entering backwards through
    #                               end and leaving backwards through start
    backward: bool = False


    
@dataclass
class SnarlTraversal:
    # Steps of the walk through a Snarl, including the start and end nodes. If the
    # traversal includes a Visit that represents a Snarl, both the node entering the Snarl
    # and the node leaving the Snarl should be included in the traversal.
    name: str
    walk: List[Visit]
    samplePaths: List[str]
    is_nested: int = 0

    
@dataclass
class Snarl:
    """Class for keeping track of snarls and traversals"""
    #{"directed_acyclic_net_graph": true, "end": {"node_id": "8455"}, "start": {"node_id": "8453"}, "start_end_reachable": true, "type": 1}
    #"directed_acyclic_net_graph": true, "end": {"node_id": "9699"}, "parent": {"end": {"backward": true, "node_id": "8279"}, "start": {"backward": true, "node_id": "8280"}}, "start": {"node_id": "9697"}, "start_end_reachable": true, "type": 1}  
    
    ID: str
    Ltip: str
    Rtip: str

    traversals: List[SnarlTraversal]
    s_type: int = 0   #UNCLASSIFIED = 0, ULTRABUBBLE = 1, UNARY = 2 
    parentSnarl: str = None   #if nested snarl, this holds the parent snarl's id 
    nTraversals: int = 0  #unique traversals represent alleles
    nPaths: int = 0  # total paths that can be traced though the snarl;  can exceed total num of samples (repeats,etc.)
    start_self_reachable: bool = False
    end_self_reachable: bool = False
    start_end_reachable: bool = True
    directed_acyclic_net_graph: bool = True

    def add_traversal(self, trav):
        assert(isinstance(trav, SnarlTraversal)) 
        self.traversals.append(trav)
        self.nTraversals += 1

    def add_parent_snarl(self, data):
        flipParent = False
        parentStart = None
        parentEnd = None
        if 'backward' in data['parent']['start'].keys() or 'backward' in data['parent']['end'].keys():
            #"parent": {"end": {"backward": true, "node_id": "7768"}, "start": {"backward": true, "node_id": "7769"}}
            if data['parent']['start']['backward'] and data['parent']['end']['backward']:
                flipParent = True
            elif data['parent']['start']['backward'] or data['parent']['end']['backward']:
                #What to do if only one of the parent snarl's ends is in reverse orientation relative to this snarl?
                raise Exception('The outter (parent) snarl has unsupported orientation relative to current snarl')
        parentStart =  data['parent']['end']['node_id'] if flipParent else  data['parent']['start']['node_id']
        parentEnd =  data['parent']['start']['node_id'] if flipParent else  data['parent']['end']['node_id']
        assert parentStart is not None
        assert parentEnd is not None

        parentName = parentStart + '<>' + parentEnd
        self.parentSnarl = parentName

        
    def get_nodes_involved(self, includeTips=False):
        # Returns a non-redundant list of all nodes involved in the snarl
        # Whether or not to incluce the tips is controlled by the second arg
        if not self.nTraversals:
            return []
        nodes = []
        firstIdx=1
        lastIdx=-1
        if includeTips:
            firstIdx=0
            lastIdx=None
        for t in self.traversals:
            for visit in t.walk[firstIdx:lastIdx]:
                if visit.node_id not in nodes:
                    nodes.append(visit.node_id)
        return nodes
    

class VarTypes(enum.Enum):
    Ins : 1
    Del : 2
    Inv : 3
    Rel : 4


        
class SnarlsDF:
    """
    Centers arond a pandas data frame contating snarl data. All the data is obtained from a dictionary of Snarl objects.
    (Nore: individulal snarl traversals are outside of this class's scope. See TravsDF instead) 
    """
    def __init__(self, snarlInfoD, gfa):

        self.df = pd.DataFrame()
        # Do we want to count the snarl tips as part of the "bubble"?
        includeTips = False
        for s in snarlInfoD.values():
            bubbleNodes = s.get_nodes_involved(includeTips)
            bubbleSize = 0
            #nodes used in this context are non-redundant, i.e. the size will not depend on the number of traversals(alleles) - only on the unique nodes involved!
            for name in bubbleNodes:
                assert gfa.segment(name)
                bubbleSize += gfa.segment(name).length

            data = {
                'snName' : s.ID,
                'type' : s.s_type,
                'b-tip_L' : s.Ltip,
                'b-tip_R' : s.Rtip,
                'parent_snarl' : s.parentSnarl,
                'nTraversals' : s.nTraversals,
                'nPaths' : s.nPaths,
                'bubble_nodes' : ':'.join(bubbleNodes),
                'bubble_size' : bubbleSize
            }
            self.df = self.df.append(data, ignore_index=True)
            #all num values are floats by default; convert to int
        self.df['type'] = self.df['type'].astype(int)
        self.df['nTraversals'] = self.df['nTraversals'].astype(int)
        self.df['nPaths'] = self.df['nPaths'].astype(int)
        self.df['bubble_size'] = self.df['bubble_size'].astype(int)
        
    def print_to_csv(self, prefix):
        outFn = prefix+'.snarls.csv'
        self.df.to_csv(outFn)
        
    def debug(self):
        print(self.df.shape)
        print(self.df.head(10))



class TravsDF:
    """
    Centers arond a pandas data frame contating individual snarl traversal data. All the data is obtained from either a gfa2 object, a csv file,  or from a dictionary of Snarl objects.
    (Nore: individulal snarl traversals are outside of this class's scope. See TravsDF instead) 
    """

    def __init__(self, arg):
        self.df = pd.DataFrame()
        if isinstance(arg, gfapy.Gfa):
            assert(arg.version == 'gfa2')
            self.construct_from_gfa(arg)            
        elif isinstance(arg, dict):
            key, val = random.choice(list(arg.items())) 
            assert(isinstance(val, Snarl))
            self.construct_from_snarlsD(arg)
        elif isinstance(arg, str):
            self.construct_from_csv(arg)

    def construct_from_csv(self, traversalsCSV):
        self.df = pd.read_csv(traversalsCSV, index_col=0)
        
    def construct_from_snarlsD(self,snarlInfoD):
        raise Exception('Construction from a snarl dictionary is not yet implemented..  I will do it right now!')
    
    def construct_from_gfa(self,gfa2):
        accessionsL = get_accessions(gfa2)
        nAccessions = len(accessionsL)
        assert(nAccessions)
        for p in gfa2.paths:
            if p.sn:
                #look as all paths that have a (sn) tag, i.e. they are snarl traversal walks 
                #O       sn-trav.8506<>8507_2       8506+ 8507+     sn:Z:8506<>8507 pp:Z:pBd21_3.7,pBd30_1.14,pRef.2        np:i:3  nd:i:0            
                data = {
                    'snarlName' : p.sn,
                    'travName' : p.name,
                    'isNested' : p.nd,
                    'travPathNames' : p.pp 
                }
                support = 0
                for accn in accessionsL:
                    if accn in p.pp:
                        nodeNames = [item.name for item in p.items]  #storing wihtout orientation info
                        #To make a traversal equivalent to an allele we need to ignore the "tips", which means it can end up with zero elements, e.g. as in a deletion)
                        nodeNames = nodeNames[1:-1]  # strip the "tips"  
                        alleleLen = 0
                        for n in nodeNames:
                            alleleLen += gfa2.segment(n).length
                        data[accn] = alleleLen
                        support += 1  #Note, we only count an accession once, even if multiple traversals exist matching the same accession  (we assume only one of them is correct)
                    else:
                        data[accn] = None
                    wgt = support/nAccessions
                    data['alleleWeight'] = round(wgt,2)
                        
                self.df = self.df.append(data, ignore_index=True)

    def get_df(self):
        return self.df
    
    def print_to_csv(self, prefix):
        outFn = prefix+'.travs.csv'
        self.df.to_csv(outFn)

    def debug(self):
        print(self.df.shape)
        print(self.df.head(10))
        #print('count:\n', self.df.count(), '\n')
        #print('means:\n', self.df.mean(), '\n')
        #print('sums:\n', self.df.sum(axis = 0, skipna = True), '\n')



def get_accessions(gfa):
    """
    If path lines contain the 'ac' tag, take its value as the sample/accession name, 
    otherwise extract them from path names assuming this convention:  p[accession].[0-9]*.
    Tags 'sn' indicate snarl traversal paths which get skipped
    Both GFA1 and GFA2 work as agrs
    
    """

    accessionsL = []
    if gfa.paths[0].ac:
        sys.stderr.write("Will read sample/accession names from the paths' \"ac\" tags\n")
    else:
        sys.stderr.write("Will extract sample/accession names from from path names assuming this convention:  p[accession].[0-9]*\n")

    for p in gfa.paths:
        if p.sn:
            continue
        if p.ac:
            accession = p.ac
        else:
            accession = p.name.split('.')[0]
            accession = accession[1:] # strip the leading 'p'
        if accession not in accessionsL:
            accessionsL.append(accession)
    return accessionsL


def all_paths_as_str(gfa):  #both GFA1 and GFA2 work
    pathStrings = {}
    for p in gfa.paths:
        segmentsL = [ str(s.name+s.orient) for s in p.captured_segments ]
        segStr = ' '.join(segmentsL)
        pathStrings[p.name] = segStr
    return pathStrings

def all_paths_as_DofL(gfa):
    '''
    Consructs a dictionary of lists containing all paths in the gfa object (GFA1 or GFA2) as lists of oriented segments ( e.g. {pFoo.1 : [s1+,s2+,s5-]} )
    '''
    paths = {}
    for p in gfa.paths:
        #go from orientedLine type to  list of name+ori
        oriNames = []
        for s in p.captured_segments:
            oriNames.append(str(s.name+s.orient))
            paths[p.name] = oriNames.copy()
    return paths


def extract_path_subgraph(gfa, pathLines, gfaOutVersion):
    '''
    !!!INCOMPLETE!!!
    Builds a gfa graph defined by segments and edges of a GFA P-line or GFA2 O-line.
    Paths are passed in as gfapy lines. The path lines are first added to the new graph and 
    are then  parsed by methods captured_segments() and captured_edges()
    to extract segment and edge data and then add it to the new graph as lines.
    Creates a temporary graph in gfa1 format, then, if necessary, converts to gfa2
    '''
    gfaSubgraph = gfapy.Gfa(version='gfa1')

    if pathLines[0].startswith('P'):
        for l in pathLines:
            gfaSubgraph.add_line(l)
    elif pathLines[0].startswith('O'):
        pass
    else:
        raise Exception('Unsupported path line; Expecting P or O')
    
    #The P lines are present as path objects in the new graph. Now we add the segments they contain
    # for p in gfaSubgraph.paths:
    #     for seg in p.captured_segments:
    #         gfaSubgraph.add_line(seg.line)
    #     for edge in p.captured_edges:
    #         gfaSubgraph.add_line(edge.line)   
    #     print()
    return gfaSubgraph


def path_is_graph_coherent(els, gfa, maxRank=None):
    #expect elements to begin with direction prefix
    if els[0][0] != '>' and els[0][0] != '<':
        raise Exception('Invalid start of path element; Expecting direction')
    for e in els:
        eName = e[1:] #remove original direction prefix
        if eName not in gfa.names:
            print('path segment ', eName, ' is missing from the graph')
            return 0
        if maxRank and gfa.segment(eName).SR > maxRank:
            print('path segment ', eName, ' is not coherent with the specified rank limit (', maxRank, ')')
            return 0
            
    return 1



def trim_path_to_graph(elements,gfa):
    '''
    Trims walk elements that are not found in the graph. The trimming is done from the terninal ends, and 
    as soon as a gfaph-connected segment is found the trimming stops.  The walk is  passed in as a list of segment names coupled with orientation. 
    '''
    trimStart = 0
    trimEnd = 0
    trimmed = []
    if elements[0][0] != '>' and elements[0][0] != '<':
        raise Exception('Invalid start of walk element; Expecting direction')
    # look for the first element that's present in the graph
    i = 0
    while i < len(elements) :
        eName = elements[i][1:] #remove original direction prefix
        if eName in gfa.names:
            trimStart = i
            break
        else:
            i += 1
            
    # now look from the other end
    i = len(elements) - 1
    while i > trimStart:
        eName = elements[i][1:] #remove original direction prefix
        if eName in gfa.names :
            trimEnd = i+1
            break
        else:
            i -= 1
            trimmed = elements[trimStart:trimEnd]
    return trimmed


def path_to_seq(pathName,gfa):
    mySeq = ''
    for cs in gfa.line(pathName).captured_segments:
        assert cs.sequence
        if cs.orient == '-':
            mySeq += rc(cs.sequence)
        else:
            mySeq += cs.sequence
    return mySeq




### GENERIC        
def list_A_contained_in_B(A, B):
    n = len(A)
    return any(A == B[i:i + n] for i in range(len(B)-n + 1)) 

def get_bins(nBins,total):
    """
    breaks up 'total' into bin containders of size total/nBins; returns a list of lists, e.g
     [[0, 5.0], [5.0, 10.0], [10.0, 15.0], [15.0, 20.0]]
    """
    bin_size = total / nBins
    bins = [(i * bin_size, (i + 1) * bin_size) for i in range(nBins)]
    if not bin_size.is_integer():
        bins =   [ [round(bin[0],2), round(bin[1],2)]  for bin in bins]
    return bins



class BubbleOperations:

    def extract_bubble_paths(self, gfa, bubbleTips):
        # Scans existing graph paths for the given pair of bubbleTips and if found, extracts all segments between the bubbleTip as
        # and build a bPath. Retruns all bPaths found
        
        bPathLines = []
        for samplePath in gfa.paths:
            bPathStart = bPathEnd = None
            for i, oriSeg in enumerate(samplePath.captured_segments):
                if oriSeg.name== bubbleTips[0] or oriSeg.name == bubbleTips[1]:
                    if not bPathStart:
                        bPathStart = i
                    elif not bPathEnd:
                        bPathEnd = i
                if bPathStart and bPathEnd:
                    break            
            if bPathStart and bPathEnd:
                bSegments = samplePath.captured_segments[bPathStart:bPathEnd+1]
                oriNames = []
                for s in bSegments:
                    oriNames.append(str(s.name + s.orient))
                    bSegString = ','.join(oriNames)
                    bPathName = samplePath.name + '_bubble-path'
                    bPathLine = gfapy.Line('P\t' + bPathName + '\t' + bSegString + '\t*')
                    print('bPathLine: ',bPathLine)
                    bPathLines.append(bPathLine)
        return bPathLines

    def extract_bubble_subgraph(self, gfa, bubbleTips, gfaOutVersion):
        bPathLines = self.extract_bubble_paths(gfa, bubbleTips)
        subgraph = extract_subgraph(bPathLines, gfaOutVersion)
        return subgraph
    

def vg_snarls_to_snarlInfoD(snarlsFile):
    """
    Extracts snarl data from the input json snarls file (output of vg snarls | vg view) and populates the 
    snarlInfo dict with Snarl objects
    """
    snarlInfoD = {}
    for line in open(snarlsFile, "r"):
        data=json.loads(line)
        if data['start']['node_id'] and data['end']['node_id']:
            snarlID=data['start']['node_id'] + '<>' + data['end']['node_id']
            newSnarl = Snarl(snarlID, data['start']['node_id'], data['end']['node_id'], [])
        else:
            raise Exception('unrecognized format of snarl line:\n',line)

        if 'type' in data:
            newSnarl.s_type = data['type']
        if 'parent' in data:
            newSnarl.add_parent_snarl(data)
            if newSnarl.parentSnarl not in snarlInfoD:
                print('warning! parent snarl name hasn\'t been seen yet!\n',line)

        if not ('directed_acyclic_net_graph' in data and data['directed_acyclic_net_graph'] == True):
            newSnarl.directed_acyclic_net_graph = False
        if 'start_self_reachable' in data and data['start_self_reachable'] == True:
            newSnarl.start_self_reachable = True
        if 'end_self_reachable' in data and data ['end_self_reachable'] == True:
            newSnarl.end_self_reachable = True
        if not ('start_end_reachable' in data and data['start_end_reachable'] == True):
            newSnarl.start_end_reachable = False

        snarlInfoD[snarlID] = newSnarl

    n=len(snarlInfoD)
    print(f"Parsed {n} snarls")

    return snarlInfoD




def parse_vg_traversals(traversalsFile, snarlInfo):
    """
    Extract tranversal info from input json file (output of vg snarls -r | vg view ) and construct
    a SnarlTraversal object, then add it to the corresponding snarl in the snarlInfo dict
    """
    n=0
    totalNodesInTraversals = 0
    for line in open(traversalsFile, "r"):
    
        data=json.loads(line)

        # so far I've only seen traversals using just a list of 'node id' items.  The vg.proto file suggests that these may contain nested snarl ids as well.  I will code these in once I see and example
        if data['visit'][0]['node_id'] and data['visit'][-1]['node_id']:
            snarlID=data['visit'][0]['node_id'] + '<>' + data['visit'][-1]['node_id']
        else:
            raise Exception('unrecognized format of traversal line:\n',line)
    
        if snarlID  not in snarlInfo:
            print('WARNING: snarl id ', snarlID,' found in traverals file is missing from the snarls file ; Will define this snarl using the minimal info implied by the traversal..')
            start = Visit(node_id = data['visit'][0]['node_id'])
            end = Visit(node_id = data['visit'][-1]['node_id'])
            newSnarl = Snarl(snarlID, start, end, [])
            snarlInfo[snarlID] = newSnarl

        mySnarl = snarlInfo[snarlID]
        mySnarl.nPaths += 1 #add this traversal to the snarl's path count regardless of its composition or uniqueness

        #now analyze the traversal itself
        t_name = 'sn-trav.' + snarlID + '_' + str(mySnarl.nTraversals + 1)
        myTrav = SnarlTraversal(t_name, [], [])
        if mySnarl.parentSnarl:
            myTrav.is_nested = 1
        for i in range(len(data['visit'])):
            myVisit = Visit(node_id = data['visit'][i]['node_id'])
            if 'backward' in data['visit'][i]:
                myVisit.backward = True
            myTrav.walk.append(myVisit)

        #The same traversal can be listed multiple times for the same snarl (e.g. when matching multiple paths);
        redundant=False
        if len(mySnarl.traversals):
            for t in mySnarl.traversals:
                if t.walk == myTrav.walk:
                    redundant=True
                    #print('\tSkipping redundant traversal ', t.walk)
                    break
        if not redundant:
            mySnarl.add_traversal(myTrav)
            #print('Added traversal ', snarlInfo[snarlID].traversals[-1].walk)
            n += 1
            totalNodesInTraversals += len(snarlInfo[snarlID].traversals[-1].walk)

    print(f'Added {totalNodesInTraversals} nodes in {n} unique traversals')
