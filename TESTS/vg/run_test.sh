#!/bin/bash

#To validate that the variant calling workflow is configured properly, run this on a small GFA file as produced by construct_graph.minigraph.sh 

run_verbose()
{
    >&2 echo -e "\n##### Executing  \"$@\"\n"
    eval $@
}

pipeline=/global/homes/e/eugeneg/scripts/pangenome_graph/variant_analysis/call_variants_on_GFA.sh
gfaFile=./test_with_1bubble.GFA
refPathPrefix='pLimC1'
contigPathPrefix='pLimO1'


run_verbose $pipeline $gfaFile $refPathPrefix $contigPathPrefix

if [ -f "$gfaFile.numIds.vcf" ]; then echo "Test successful."; else echo "Test failed.";
fi
