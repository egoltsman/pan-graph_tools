
#source activate my_env
#S1 is the "reference" chr1;  create a file listing chromosomes and sizes (one in this case)
#samtools faidx ~/PANGENOME_GRAPHS/Simulations/Visor/BdistachyonRef_chr1.fa | cut -f1,2 > chrom.dim.tsv


source activate visor
module load R/3.6.1-conda


for i in {2..14}; do
    sample=S$i
    ( Rscript ~/utils/VISOR/scripts/randomregion.r -d chrom.dim.tsv -n 10000 -l 1 -v 'deletion,inversion,inverted tandem duplication,translocation copy-paste,reciprocal translocation' -r '20:20:20:20:20' > ${sample}.tmp.bed &&
      sed -i 's|Setting OSU repository||g' ${sample}.tmp.bed &&
      sortBed -i ${sample}.tmp.bed > ${sample}.random.bed &&
      rm ${sample}.tmp.bed &&
      VISOR HACk -g BdistachyonRef_chr1.fa -bed $sample.random.bed -o $sample.haplotypes &&
      sed "s|>.*|>sample$sample|" $sample.haplotypes/h1.fa > ./${sample}.h1.fa  & )
done
