#assume rGFA input, in which case we need to check for presense of header line and strip off leading 's' from segment ids
if [[ `head -n 1 $1` != H* ]]; then
    echo -e 'H\tVN:Z:1.0\n' > ${1}.numIds 
fi
sed 's|s\([0-9]*\)|\1|g' $1  >> ${1}.numIds
vg view -F -v ${1}.numIds > ${1}.vg
