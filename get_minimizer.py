#! /usr/bin/env python3

import os, getopt, sys
from pympler.asizeof import asizeof

sys.path.append('/global/homes/e/eugeneg/scripts')
from timer import Timer

from collections import defaultdict

script=os.path.basename(sys.argv[0])
usageStr='Usage: ' + script + ' -f <sequence_file>  << -w <window_size> >>  << -m <minimizer_size> >>  << -P(rofile memory)? >>    ' 

if len(sys.argv) < 2:
    print(usageStr)
    sys.exit(2)


Win = 31
M = 7
profileMem = 0
t = Timer()
    
opts, extras = getopt.getopt(sys.argv[1:], 'f:w:m:P')
for opt, arg in opts:
    if opt == '-f':
        seqFile = arg
    if opt == '-w':
        Win = int(arg)
    if opt == '-m':
        M = int(arg)
    if opt == 'P':
        profileMem = 1
outFn = seqFile.minimerCount + '.w' + Win + '.m' + M



def get_minimizers(M, Win, seq, minimizerCountsD):
    '''
    determine minimizers in the sequence and accumulate them and their counts in the passed in dictionary.  The dictionaly may be already populated at call time, e.g. we're counting  minimizers across many sequences
    '''
    t.start()
    if any(c.islower() for c in seq):
        seq = seq.upper()
    
    rev=seq[::-1]

    rev=rev.replace("A","X")
    rev=rev.replace("T","A")
    rev=rev.replace("X","T")
    rev=rev.replace("C","X")
    rev=rev.replace("G","C")
    rev=rev.replace("X","G")

    L=len(seq)
    minPrev=''
    mCnt = 0
    for i in range(0, L-Win+1):

        sub_f=seq[i:i+Win]
        sub_r=rev[L-Win-i:L-i]

        min="ZZZZZZZZZZZZZZZZZZZZZZZZZZZZ"
        for j in range(0, Win-M+1):
            sub2=sub_f[j:j+M]
            if sub2 < min:
                min=sub2
            sub2=sub_r[j:j+M]
            if sub2 < min:
                min=sub2
        if min != minPrev:
            minimizerCountsD[min] += 1
            minPrev = min
            mCnt += 1
            if mCnt % 100000 == 0:  #print status after every 100k minimizers
                print(i, mCnt, flush=True)

    if profileMem:
        of.write('dict len: ',len(minimizerCountsD), ' dict size: ', asizeof(minimizerCountsD), ' bytes',  flush=True)
        
    t.stop()

    
#TEST
#testD=defaultdict(int)
#get_minimizers(7, 31, "ATGCGATATCGTAGGCGTCGATGGAGAGCTAGATCGATCGATCTAAATCCCGATCGATTCCGAGCGCGATCAAAGCGCGATAGGCTAGCT#AAAGCTAGCA", testD)
#assert( len(testD) == 7 )


if __name__ == "__main__":

    minimizerCounts = defaultdict(int)
    nMins = 0
    nLocs = 0
    maxCnt = 0

    of = open(outFN, 'x')
    
    with open(seqFile) as f:
        seq=''
        for line in f:
            if line.startswith('>'):
                if len(seq):
                    get_minimizers(M, Win, seq, minimizerCounts)
                    seq=''
                print(line.rstrip(), flush=True)
            else:
                seq += line.rstrip()
        if len(seq):
            get_minimizers(M, Win, seq, minimizerCounts)

            
      
    for v in minimizerCounts.values():
        nMins += 1
        nLocs += v
        if v > maxCnt:
            maxCnt = v
            
    of.write('nMins: ', nMins, ' nLocs: ', nLocs, ' maxCnt: ', maxCnt)
        
