#!/bin/bash


#given a vcf file (from vg deconstruct), extract indels occurring on the given reference path that match the size constraint,  then output as fasta

## IMPORTANT! the vcf file must have the INFO column containing SVTYPE=*;SVLEN=*

vcfFile=$1
min=$2
max=$3
path=$4  #reference path 
sampleCol=$5 #column number containing genotypes of the sample of interest

if [ "$#" -ne 5 ]; then
    echo "Illegal number of parameters"
    echo "Usage: $0 vcfFile min max refPath smplCol"
    exit
fi

echo $path
svlenFile=${vcfFile}.svlen
if [ ! -s $svlenFile ]; then
    ~/scripts/pangenome_graph/variant_analysis/get_indel_sizes.sh $vcfFile $sampleCol > $svlenFile
    if [ ! -s $svlenFile ]; then
	echo "Couldn't get any indel size data"
	exit
    fi
fi

if [ ! -s ${path}.${min}-${max}_pos ]; then
    #get all indel positions withing the min-max range on the ref path
    grep -w $path $svlenFile | awk -v OFS='\t' -v minsize=$min -v maxsize=$max 'BEGIN {minNeg = maxsize*-1; maxNeg = minsize*-1 } {if (($3>=minNeg && $3<=maxNeg) || ($3>=minsize && $3<=maxsize)) {print $1, $2}}' > ${path}.${min}-${max}_pos
fi



for p in `cut -f2 ${path}.${min}-${max}_pos`; do
    #FIXME!! In case of an insertion, this will always print the *combined" alt allele which could include multiple sequences separated by commas. Need to add logic to extract the sequence matching the specific sample
    grep -w $p $vcfFile | awk -v refpath=$path '{if (length($4) > length($5)) {print ">"refpath"_"$2".DEL\n",$4} else {print ">"refpath"_"$2".INS\n",$5} }' >> ${path}.${min}-${max}.fasta
done
    
if [ -s ${path}.${min}-${max}.fasta ]; then
    echo 'Done'
else
    echo "Couldn't extract any indel sequences"
fi

