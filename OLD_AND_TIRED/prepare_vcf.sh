#!/bin/bash
set -e

#Prepare variant calls in the vcf file output by 'vg deconstruct' for downstream use

vcf_in=$1

#Add offset to reference  
if [[ $# > 1 ]]; then
    cat $vcf_in   | awk -v offset=$offset 'BEGIN {OFS="\t"} {if ($1 ~ /\.2/){$2+=offset} print}' > ${vcf_in}.offset.vcf
    vcf_in=${vcf_in}.offset.vcf     
fi

(grep '#' $vcf_in && grep -v '#' $vcf_in| sort -k1,1 -k2,2n) > ${vcf_in}.sort

#strip colons and anything that follows up to the next field
sed  's|:[[:digit:]]\+-[[:digit:]]\+\t|\t|g'  $vcf_in.sort | sed 's|:[[:digit:]]\+-[[:digit:]]\+,|,|g' > $vcf_in.sort.sed

#add SVTYPE and SVLEN records
#!!!! SVLEN is taken as the difference between the reference and the *first* alternate allele
cat $vcf_in.sort.sed  | awk 'BEGIN {OFS="\t"} /^#/ {print; next} {altEnd=index($5,",")-1; alt=(altEnd == -1 ? $5 : substr($5,0,altEnd)); svlen=length(alt)-length($4); svtype = svlen < 0 ? "DEL" : "INS";  $8="SVTYPE="svtype";SVLEN="svlen}{print}'  > $vcf_in.sort.fixed
info1=`echo "##INFO=<ID=SVTYPE,Number=1,Type=String,Description="Type of structural variant" "`
info2=`echo "##INFO=<ID=SVLEN,Number=.,Type=Integer,Description="Difference in length between REF and ALT" "`
sed -ie "/^##fileformat/a $info1\n$info2" $vcf_in.sort.fixed    #add after ##fileformat header line

#bcftools view -V snps $vcf_in.sort.fixed > $vcf_in.sort.fixed.NO-SNPS.vcf
bgzip $vcf_in.sort.fixed
tabix -p vcf $vcf_in.sort.fixed.gz

rm $vcf_in.sort.sed
rm $vcf_in.sort.fixed
rm $vcf_in.sort.fixede

echo "All done."
