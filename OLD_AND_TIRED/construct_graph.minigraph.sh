#!/bin/bash
set -e


run_verbose()
{
    >&2 echo -e "\n##### Executing \"$@\"... #####\n"
    eval $@
}



fastasStr=$1
declare -A sNames

#optionally, include an existing base GFA file to augment
if [ "$#" -eq 2 ]; then
    baseGfa=$2
fi

interactive=1  #For a single multi-fasta file, ask whether to split it;

if [ -z "$UTILS_PATH" ]; then
    UTILS_PATH=/global/homes/e/eugeneg/utils
fi

if [ -z "$SCRIPTS_PATH" ]; then
    SCRIPTS_PATH=/global/homes/e/eugeneg/scripts/pangenome_graph
fi

nFastas=`echo $fastasStr | wc -w`

if [[ ! $nFastas -gt 0 ]]; then
    echo "ERROR: Invalid input args (no sample fastas)"
    exit
fi

if [[ $nFastas -gt 1 ]]; then
    gfaOut="${nFastas}_genomes.GFA"
else
    gfaOut=`echo $fastasStr | sed 's|\.fa.*||'`  #strips .fa or .fasta
    gfaOut=$gfaOut.GFA
fi



if [[ -n $baseGfa ]]; then
    inputStr="$baseGfa $fastasStr"
else
    inputStr=$fastasStr
fi


if [[ $nFastas -eq 1 ]]; then
    nRecords=`grep -c '>' $fastasStr`
    #split the all-in-one fasta into individual files
    if [[ ! -z $interactive ]] ; then
	if [[ $nRecords -gt 1 ]]
	then
	    while true; do
		read -p "Input fasta contains $nRecords entires. Do you want to split it and treat each entry as an individual sample?" yn
		case $yn in
		    [Yy]* ) run_verbose /global/homes/e/eugeneg/scripts/formatters_and_filters/splitfasta.pl $fastasStr 1 && inputStr=`ls ${inputStr}_*[0-9] | tr "\n" " " ` \
				  && echo "Split resulted in files $inputStr";
			    read -r -a fastas <<< $inputStr
			    for f in "${fastas[@]}"; do
				read -p "Enter sample name for ${f}:" name 
				sNames[$f]=$name
			    done
			    break;;
		    [Nn]* ) echo "Will process multiple fasta records as part of one sample, i.e. won't align fasta entries against each other";
			    break;;
		    * ) echo "Please answer yes or no.";;
		esac
	    done
	    
	fi
    elif [[ -n $splitFasta ]] ; then
	run_verbose /global/homes/e/eugeneg/scripts/formatters_and_filters/splitfasta.pl $fastasStr 1 && inputStr=`ls ${inputStr}_*[0-9] | tr "\n" " " ` && echo "Split resulted in files $inputStr";
    fi
    
else
    echo "$nFastas fasta files passed in - will treat as separate samples."
    read -r -a fastas <<< $fastasStr
    for f in "${fastas[@]}"; do
	  read -p "Enter sample name for ${f}:" name 
	  sNames[$f]=$name
    done
fi

#construct 
run_verbose ${UTILS_PATH}/minigraph/minigraph -x ggs -t 36 $inputStr > $gfaOut

cp $gfaOut $gfaOut.noPaths
gaFOutPref=`echo $gfaOut | sed 's|GFA|gaf|'`

#add paths iteratively, overwriting previous gfa file with new one
for fasta in "${!sNames[@]}"; do
    sampleName="${sNames[$fasta]}"
    #map-back
    echo 
    echo "Mapping $fasta (sample $sampleName) back to the graph.."
    gaFOut=$gaFOutPref.$sampleName
    run_verbose ${UTILS_PATH}/minigraph/minigraph -t 36 --vc -x asm $gfaOut $fasta  > $gaFOut

    #add paths

    run_verbose ${SCRIPTS_PATH}/add_gaf_paths_to_gfa.py -a $gaFOut -g $gfaOut -o $gfaOut -m 10 -N $sampleName
done

echo "Done."
